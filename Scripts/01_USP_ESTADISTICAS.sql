CREATE PROCEDURE covid.USP_ESTADISTICAS
(
	@PI_FECHA DATE = NULL,
	@PI_DEPARTAMENTO_ID INT = NULL,
	@PI_PROVINCIA_ID INT = NULL,
	@PI_DISTRITO_ID INT = NULL
)
AS
BEGIN
	SET @PI_FECHA = COALESCE(@PI_FECHA, GETDATE())

	SELECT
		EST.Nombre,
		COUNT(C.EstadoId) AS Cantidad
	FROM COVID.Ciudadano C
	INNER JOIN covid.Direccion DIR ON DIR.CiudadanoId = C.CiudadanoId
	INNER JOIN common.Distrito DIS ON DIS.DistritoId = DIR.DistritoId
	INNER JOIN common.Provincia PROV ON PROV.ProvinciaId = DIS.ProvinciaId
	INNER JOIN common.Departamento DEP ON DEP.DepartamentoId = PROV.DepartamentoId
	INNER JOIN covid.Estado EST ON EST.EstadoId = C.EstadoId
	WHERE
		CAST(C.Audit_FechaCreacion AS DATE) = @PI_FECHA AND
		(@PI_DEPARTAMENTO_ID IS NULL OR (DEP.DepartamentoId = @PI_DEPARTAMENTO_ID)) AND
		(@PI_PROVINCIA_ID IS NULL OR (PROV.ProvinciaId = @PI_PROVINCIA_ID)) AND
		(@PI_DISTRITO_ID IS NULL OR (DIS.DistritoId = @PI_DISTRITO_ID))
	GROUP BY EST.Nombre
END	
GO