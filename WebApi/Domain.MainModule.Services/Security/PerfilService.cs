﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Security;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Security;
using Domain.MainModule.Contracts.Services.Security;

namespace Domain.MainModule.Services.Security
{
    public class PerfilService : GenericService<Perfil>, IPerfilService
    {
        public PerfilService(IUnitOfWorkFactory unitOfWorkFactory,
                             IPerfilRepository perfilRepository)
            : base(unitOfWorkFactory, perfilRepository)
        {
        }
    }
}
