﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;

namespace Domain.MainModule.Services.Covid19
{
    public class SituacionRiesgoService : GenericService<SituacionRiesgo>, ISituacionRiesgoService
    {
        public SituacionRiesgoService(IUnitOfWorkFactory unitOfWorkFactory, ISituacionRiesgoRepository situacionRiesgoRepository) : base(unitOfWorkFactory, situacionRiesgoRepository)
        {
        }
    }
}
