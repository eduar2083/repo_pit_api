﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;

namespace Domain.MainModule.Services.Covid19
{
    public class EstadoService : GenericService<Estado>, IEstadoService
    {
        public EstadoService(IUnitOfWorkFactory unitOfWorkFactory, IEstadoRepository estadoRepository) : base(unitOfWorkFactory, estadoRepository)
        {
        }
    }
}
