﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Covid19
{
    public class CiudadanoService : GenericService<Ciudadano>, ICiudadanoService
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private readonly ICiudadanoRepository ciudadanoRepository;
        private readonly IContactoRepository contactoRepository;
        private readonly IDireccionRepository direccionRepository;
        private readonly ISintomaRepository sintomaRepository;
        private readonly ISituacionRiesgoRepository situacionRiesgoRepository;

        public CiudadanoService(IUnitOfWorkFactory unitOfWorkFactory,
                                ICiudadanoRepository ciudadanoRepository,
                                IContactoRepository contactoRepository,
                                IDireccionRepository direccionRepository,
                                ISintomaRepository sintomaRepository,
                                ISituacionRiesgoRepository situacionRiesgoRepository) : base(unitOfWorkFactory, ciudadanoRepository)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.ciudadanoRepository = ciudadanoRepository;
            this.contactoRepository = contactoRepository;
            this.direccionRepository = direccionRepository;
            this.sintomaRepository = sintomaRepository;
            this.situacionRiesgoRepository = situacionRiesgoRepository;
        }

        public async Task<int> AgregarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo)
        {
            try
            {
                using (var uow = unitOfWorkFactory.Create())
                {
                    ciudadanoRepository.Add(ciudadano);

                    foreach (var c in contactos)
                    {
                        c.Ciudadano = ciudadano;
                        contactoRepository.Add(c);
                    }

                    foreach (var d in direcciones)
                    {
                        d.Ciudadano = ciudadano;
                        direccionRepository.Add(d);
                    }

                    foreach (var s in sintomas)
                    {
                        s.Ciudadano = ciudadano;
                        sintomaRepository.Add(s);
                    }

                    foreach (var sr in situacionesRiesgo)
                    {
                        sr.Ciudadano = ciudadano;
                        situacionRiesgoRepository.Add(sr);
                    }

                    int result = await uow.CommitAsync();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> ActualizarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo)
        {
            try
            {
                using (var uow = unitOfWorkFactory.Create())
                {
                    var e_ciudadano = await ciudadanoRepository.FindAsync(ciudadano.CiudadanoId);
                    e_ciudadano.Nombre = ciudadano.Nombre;
                    e_ciudadano.ApellidoPaterno = ciudadano.ApellidoPaterno;
                    e_ciudadano.ApellidoMaterno = ciudadano.ApellidoMaterno;
                    e_ciudadano.Sexo = ciudadano.Sexo;
                    e_ciudadano.NacionalidadId = ciudadano.NacionalidadId;
                    e_ciudadano.TipoDocumentoIdentidadId = ciudadano.TipoDocumentoIdentidadId;
                    e_ciudadano.NroDocumentoIdentidad = ciudadano.NroDocumentoIdentidad;
                    e_ciudadano.Celular = ciudadano.Celular;
                    e_ciudadano.Correo = ciudadano.Correo;
                    e_ciudadano.EstadoId = ciudadano.EstadoId;
                    ciudadanoRepository.Update(e_ciudadano);

                    foreach (var c in contactos)
                    {
                        var e_contacto = await contactoRepository.FindAsync(c.ContactoId);
                        e_contacto.NombreContacto = c.NombreContacto;
                        e_contacto.Parentesco = c.Parentesco;
                        e_contacto.Telefono = c.Telefono;
                        e_contacto.Correo = c.Correo;
                        contactoRepository.Update(e_contacto);
                    }

                    foreach (var d in direcciones)
                    {
                        var e_direccion = await direccionRepository.FindAsync(d.DireccionId);
                        e_direccion.DistritoId = d.DistritoId;
                        e_direccion.Descripcion = d.Descripcion;
                        e_direccion.Ubicacion = d.Ubicacion;
                        direccionRepository.Update(e_direccion);
                    }

                    foreach (var s in sintomas)
                    {
                        var e_sintoma = await sintomaRepository.FindAsync(s.SintomaId);
                        sintomaRepository.Update(e_sintoma);
                    }

                    foreach (var sr in situacionesRiesgo)
                    {
                        var e_situacionRiesgo = await situacionRiesgoRepository.FindAsync(sr.SituacionRiesgoId);
                        situacionRiesgoRepository.Update(sr);
                    }

                    int result = await uow.CommitAsync();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> EliminarAsync(Ciudadano ciudadano)
        {
            try
            {
                using (var uow = unitOfWorkFactory.Create())
                {
                    // Obtener contactos
                    var contactos = await contactoRepository.ListFilteredWithTrackingAsync(t => t.CiudadanoId == ciudadano.CiudadanoId);

                    // Eliminar contactos
                    foreach (var c in contactos)
                    {
                        contactoRepository.Delete(c);
                    }

                    // Obtener direcciones
                    var direcciones = await direccionRepository.ListFilteredWithTrackingAsync(t => t.CiudadanoId == ciudadano.CiudadanoId);

                    // Eliminar direcciones
                    foreach (var d in direcciones)
                    {
                        direccionRepository.Delete(d);
                    }

                    ciudadanoRepository.Delete(ciudadano);

                    int result = await uow.CommitAsync();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Ciudadano> ObtenerConDetalleAsync(int CiudadanoId)
        {
            try
            {
                return await ciudadanoRepository.ObtenerConDetalleAsync(CiudadanoId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Ciudadano>> FiltrarAsync(CiudadanoCriteria c)
        {
            try
            {
                return await ciudadanoRepository.FiltrarAsync(c);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
