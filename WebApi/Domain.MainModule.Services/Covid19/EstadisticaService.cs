﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Covid19
{
    public class EstadisticaService : GenericService<Ciudadano>, IEstadisticaService
    {
        private readonly IEstadisticaRepository estadisticaRepository;

        public EstadisticaService(IUnitOfWorkFactory unitOfWorkFactory, IEstadisticaRepository estadisticaRepository) : base(unitOfWorkFactory, estadisticaRepository)
        {
            this.estadisticaRepository = estadisticaRepository;
        }

        public async Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c)
        {
            return await estadisticaRepository.FiltrarAsync(c);
        }
    }
}
