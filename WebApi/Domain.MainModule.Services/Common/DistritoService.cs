﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Common;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Common;
using Domain.MainModule.Contracts.Services.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Common
{
    public class DistritoService : GenericService<Distrito>, IDistritoService
    {
        private readonly IDistritoRepository distritoRepository;
        public DistritoService(IUnitOfWorkFactory unitOfWorkFactory,
                               IDistritoRepository distritoRepository) : base(unitOfWorkFactory, distritoRepository)
        {
            this.distritoRepository = distritoRepository;
        }

        public async Task<List<Distrito>> ListarPorProvincia(int ProvinciaId)
        {
            return await distritoRepository.ListarPorProvincia(ProvinciaId);
        }
    }
}
