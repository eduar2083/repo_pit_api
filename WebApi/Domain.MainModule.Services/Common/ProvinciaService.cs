﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Common;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Common;
using Domain.MainModule.Contracts.Services.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Common
{
    public class ProvinciaService : GenericService<Provincia>, IProvinciaService
    {
        private readonly IProvinciaRepository provinciaRepository;

        public ProvinciaService(IUnitOfWorkFactory unitOfWorkFactory,
                                IProvinciaRepository provinciaRepository) : base(unitOfWorkFactory, provinciaRepository)
        {
            this.provinciaRepository = provinciaRepository;
        }

        public async Task<List<Provincia>> ListarPorDepartamentoAsync(int DepartamentoId)
        {
            return await provinciaRepository.ListarPorDepartamentoAsync(DepartamentoId);
        }
    }
}
