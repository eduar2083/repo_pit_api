﻿using System.Collections.Generic;

namespace Domain.Core
{
    public class Paging<T>
    {
        public Paging()
        {
            TotalItems = 0;
            Items = new List<T>();
        }

        public Paging(IEnumerable<T> Items, int TotalItems)
        {
            this.Items = Items;
            this.TotalItems = TotalItems;
        }

        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

        public IEnumerable<T> Items { get; set; }
        public int TotalItems { get; set; }
    }
}
