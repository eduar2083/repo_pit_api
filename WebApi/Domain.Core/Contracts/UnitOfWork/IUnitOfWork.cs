﻿using System;
using System.Threading.Tasks;

namespace Domain.Core.Contracts.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
        Task<int> CommitAsync();
    }
}
