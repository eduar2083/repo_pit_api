﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Core.Contracts.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity item);
        IEnumerable<TEntity> Add(IEnumerable<TEntity> items);

        void Update(TEntity item);
        void Update(IEnumerable<TEntity> items);

        void Delete(TEntity item);
        void Delete(IEnumerable<TEntity> items);

        List<TEntity> ListAll();
        Task<List<TEntity>> ListAllAsync();
        Task<List<TEntity>> ListAllWithTrackingAsync();

        List<TEntity> ListFiltered(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListFilteredAsync(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListFilteredWithTrackingAsync(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListPagedAsync<S>(int pageIndex, int pageSize, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true);

        bool Any(Expression<Func<TEntity, bool>> filter = null);
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null);

        TEntity Find(params object[] keyValues);
        Task<TEntity> FindAsync(params object[] keyValues);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null);
        TEntity FirstOrDefaultWithTracking(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> FirstOrDefaulWithTrackingAsync(Expression<Func<TEntity, bool>> filter = null);

        IQueryable<TEntity> QueryableAll();
        IQueryable<TEntity> QueryableAllWithTracking();
        IQueryable<TEntity> QueryableFiltered(Expression<Func<TEntity, bool>> filter);
        IQueryable<TEntity> QueryableFilteredWithTracking(Expression<Func<TEntity, bool>> filter);
    }
}
