﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Contracts.Services;
using Domain.Core.Contracts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Core.Services
{
    public class GenericService<TEntity> : IGenericService<TEntity> where TEntity : class
    {
        private IUnitOfWorkFactory unitOfWorkFactory;
        private IGenericRepository<TEntity> genericRepository;

        public GenericService(IUnitOfWorkFactory unitOfWorkFactory, IGenericRepository<TEntity> genericRepository)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.genericRepository = genericRepository;
        }

        public virtual TEntity Add(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                var entity = genericRepository.Add(item);
                var r = uow.Commit();
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> AddAsync(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                var entity = genericRepository.Add(item);
                int r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IEnumerable<TEntity> Add(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                var entities = genericRepository.Add(items);
                var r = uow.Commit();
                return entities;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> AddAsync(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                var entities = genericRepository.Add(items);
                var r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int Update(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Update(item);
                var r = uow.Commit();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> UpdateAsync(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Update(item);
                var r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int Update(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Update(items);
                var r = uow.Commit();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> UpdateAsync(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Update(items);
                var r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int Delete(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Delete(item);
                var r = uow.Commit();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> DeleteAsync(TEntity item)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Delete(item);
                var r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int Delete(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Delete(items);
                var r = uow.Commit();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<int> DeleteAsync(IEnumerable<TEntity> items)
        {
            try
            {
                var uow = unitOfWorkFactory.Create();   // No colocar en using
                genericRepository.Delete(items);
                var r = await uow.CommitAsync();
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TEntity> ListAll()
        {
            return genericRepository.ListAll();
        }

        public virtual async Task<List<TEntity>> ListAllAsync()
        {
            return await genericRepository.ListAllAsync();
        }

        public virtual async Task<List<TEntity>> ListAllWithTrackingAsync()
        {
            return await genericRepository.ListAllWithTrackingAsync();
        }

        public List<TEntity> ListFiltered(Expression<Func<TEntity, bool>> filter)
        {
            return genericRepository.ListFiltered(filter);
        }

        public virtual async Task<List<TEntity>> ListFilteredAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await genericRepository.ListFilteredAsync(filter);
        }

        public virtual async Task<List<TEntity>> ListFilteredWithTrackingAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await genericRepository.ListFilteredWithTrackingAsync(filter);
        }

        public virtual async Task<List<TEntity>> ListPagedAsync<S>(int pageIndex, int pageSize, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true)
        {
            return await genericRepository.ListPagedAsync(pageIndex, pageSize, orderByExpression, ascending);
        }

        public bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            return genericRepository.Any(filter);
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await genericRepository.AnyAsync(filter);
        }

        public TEntity Find(params object[] keyValues)
        {
            return genericRepository.Find(keyValues);
        }

        public async Task<TEntity> FindAsync(params object[] keyValues)
        {
            return await genericRepository.FindAsync(keyValues);
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            return genericRepository.FirstOrDefault(filter);
        }

        public virtual TEntity FirstOrDefaultWithTracking(Expression<Func<TEntity, bool>> filter = null)
        {
            return genericRepository.FirstOrDefaultWithTracking();
        }

        public virtual async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await genericRepository.FirstOrDefaultAsync(filter);
        }

        public async Task<TEntity> FirstOrDefaulWithTrackingAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await genericRepository.FirstOrDefaulWithTrackingAsync(filter);
        }
    }
}
