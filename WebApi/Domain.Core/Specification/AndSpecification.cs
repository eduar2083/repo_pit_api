﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Domain.Core.Specification
{
    public sealed class AndSpecification<T> : CompositeSpecification<T>
        where T : class
    {
        private readonly ISpecification<T> leftSideSpecification;
        private readonly ISpecification<T> rightSideSpecification;

        public AndSpecification(ISpecification<T> leftSideSpecification, ISpecification<T> rightSideSpecification)
        {
            this.leftSideSpecification = leftSideSpecification;
            this.rightSideSpecification = rightSideSpecification;
        }

        public override ISpecification<T> LeftSideSpecification
        {
            get
            {
                return leftSideSpecification;
            }
        }

        public override ISpecification<T> RightSideSpecification
        {
            get
            {
                return rightSideSpecification;
            }
        }

        public override Expression<Func<T, bool>> SatisfiedBy()
        {
            Expression<Func<T, bool>> left = leftSideSpecification.SatisfiedBy();
            Expression<Func<T, bool>> right = rightSideSpecification.SatisfiedBy();

            return (left.And(right));
        }
    }
}
