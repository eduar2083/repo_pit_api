﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Core.Specification
{
    public sealed class NotSpecification<T> : Specification<T>
        where T : class
    {
        private readonly Expression<Func<T, bool>> originalCriteria;

        public NotSpecification(Expression<Func<T, bool>> originalCriteria)
        {
            this.originalCriteria = originalCriteria;
        }

        public NotSpecification(ISpecification<T> originalSpecification)
        {
            originalCriteria = originalSpecification.SatisfiedBy();
        }

        public override Expression<Func<T, bool>> SatisfiedBy()
        {
            return Expression.Lambda<Func<T, bool>>(Expression.Not(originalCriteria.Body), originalCriteria.Parameters.Single());
        }
    }
}
