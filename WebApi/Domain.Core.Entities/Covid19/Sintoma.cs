﻿namespace Domain.Core.Entities.Covid19
{
    public class Sintoma
    {
        public int SintomaId { get; set; }
        public int CiudadanoId { get; set; }
        public bool Eliminado { get; set; }

        public Ciudadano Ciudadano { get; set; }
    }
}
