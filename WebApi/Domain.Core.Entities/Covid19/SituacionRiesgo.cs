﻿namespace Domain.Core.Entities.Covid19
{
    public class SituacionRiesgo
    {
        public int SituacionRiesgoId { get; set; }
        public int CiudadanoId { get; set; }
        public bool Eliminado { get; set; }

        public Ciudadano Ciudadano { get; set; }
    }
}
