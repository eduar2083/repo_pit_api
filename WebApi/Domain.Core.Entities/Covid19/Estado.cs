﻿using System.Collections.Generic;

namespace Domain.Core.Entities.Covid19
{
    public class Estado
    {
        public int EstadoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Eliminado { get; set; }

        public ICollection<Ciudadano>  Estado_Ciudadanos { get; set; }
    }
}
