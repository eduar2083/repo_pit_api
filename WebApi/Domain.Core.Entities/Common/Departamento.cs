﻿using System.Collections.Generic;

namespace Domain.Core.Entities.Common
{
    public class Departamento
    {
        public int DepartamentoId { get; set; }
        public string Ubigeo { get; set; }
        public string Nombre { get; set; }
        public bool Eliminado { get; set; }

        public ICollection<Provincia> Provincias { get; set; }
    }
}
