﻿using Domain.Core.Entities.Covid19;
using System.Collections.Generic;

namespace Domain.Core.Entities.Common
{
    public class Distrito
    {
        public int DistritoId { get; set; }
        public string Ubigeo { get; set; }
        public string Nombre { get; set; }
        public int ProvinciaId { get; set; }
        public bool Eliminado { get; set; }

        public Provincia Provincia { get; set; }
        public ICollection<Direccion> Direcciones { get; set; }
    }
}
