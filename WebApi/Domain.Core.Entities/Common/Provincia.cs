﻿using System.Collections.Generic;

namespace Domain.Core.Entities.Common
{
    public class Provincia
    {
        public int ProvinciaId { get; set; }
        public string Ubigeo { get; set; }
        public string Nombre { get; set; }
        public int DepartamentoId { get; set; }
        public bool Eliminado { get; set; }

        public Departamento Departamento { get; set; }
        public ICollection<Distrito> Distritos { get; set; }
    }
}
