﻿using Application.Core.Contracs;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Threading.Tasks;

namespace Application.MainModule.Contracts.Services.Covid19
{
    public interface IEstadisticaAppService : IGenericAppService<Ciudadano>
    {
        Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c);
    }
}
