﻿using Application.Core.Contracs;
using Domain.Core.Entities.Covid19;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainModule.Contracts.Services.Covid19
{
    public interface IDireccionAppService : IGenericAppService<Direccion>
    {
        Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId);

        Task<List<Direccion>> ConfirmadosAsync(Point ubicacion);
    }
}
