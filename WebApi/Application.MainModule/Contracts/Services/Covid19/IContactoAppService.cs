﻿using Application.Core.Contracs;
using Domain.Core.Entities.Covid19;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainModule.Contracts.Services.Covid19
{
    public interface IContactoAppService : IGenericAppService<Contacto>
    {
        Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId);
    }
}
