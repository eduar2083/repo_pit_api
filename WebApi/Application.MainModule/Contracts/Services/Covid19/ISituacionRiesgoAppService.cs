﻿using Application.Core.Contracs;
using Domain.Core.Entities.Covid19;

namespace Application.MainModule.Contracts.Services.Covid19
{
    public interface ISituacionRiesgoAppService : IGenericAppService<SituacionRiesgo>
    {
    }
}
