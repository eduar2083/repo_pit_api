﻿namespace Infraestructure.CrossCutting.Utilities.Constantes
{
    public class SecurityConstants
    {
        public static readonly string USER_SYSTEM = "SISTEMA"; 
    }

    public class Schemas
    {
        public static readonly string SEGURIDAD = "seguridad";
        public static readonly string COMMON = "common";
        public static readonly string COVID = "covid";
    }

    public class CamposAuditoria
    {
        public static readonly string AUDIT_USUARIO_CREACION = "Audit_UsuarioCreacion";
        public static readonly string AUDIT_FECHA_CREACION = "Audit_FechaCreacion";
        public static readonly string AUDIT_USUARIO_ACTUALIZACION = "Audit_UsuarioActualizacion";
        public static readonly string AUDIT_FECHA_ACTUALIZACION = "Audit_FechaActualizacion";
        public static readonly string ELIMINADO = "Eliminado";
    }
}
