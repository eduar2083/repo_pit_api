﻿using System.Runtime.Serialization;

namespace Infraestructure.CrossCutting.Utilities.Enumerations
{
    public enum PagingDirectionEnum
    {
        [EnumMember]
        Ninguna = 0,

        [EnumMember]
        Ascending = 1,

        [EnumMember]
        Descending = 2
    }
}
