﻿namespace Infraestructure.CrossCutting.Utilities.Enumerations
{
    public enum EstadoProductoEnum
    {
        ACTIVO = 1,
        INACTIVO = 2
    }
}
