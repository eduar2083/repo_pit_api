﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Infraestructure.Data.Core.Extensions
{
    public static class QueryableExtensions
    {
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderBy");
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderByDescending");
        }

        static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;

            foreach (string prop in props)
            {
                PropertyInfo pInfo = type.GetProperty(prop);
                expr = Expression.Property(expr, pInfo);
                type = pInfo.PropertyType;
            }

            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lamba = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(method => method.Name == methodName &&
                                                                                method.IsGenericMethodDefinition &&
                                                                                method.GetGenericArguments().Length == 2 &&
                                                                                method.GetParameters().Length == 2).
                                                                            MakeGenericMethod(typeof(T), type).
                                                                            Invoke(null, new object[] { source, lamba });

            return result as IOrderedQueryable<T>;

        }
    }
}
