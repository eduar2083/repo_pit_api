﻿using Domain.Core.Contracts.Repositories;
using Infraestructure.Data.Core.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infraestructure.Data.Core.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly IDatabaseContext databaseContext;

        public GenericRepository(IDatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public virtual TEntity Add(TEntity item)
        {
            return databaseContext.Set<TEntity>().Add(item).Entity;
        }

        public IEnumerable<TEntity> Add(IEnumerable<TEntity> items)
        {
            List<TEntity> list = new List<TEntity>();
            TEntity entity;

            foreach (var item in items)
            {
                entity = Add(item);
                list.Add(entity);
            }

            return list;
        }

        public void Update(TEntity item)
        {
            databaseContext.Entry(item).State = EntityState.Modified;
        }

        public void Update(IEnumerable<TEntity> items)
        {
            foreach (var item in items)
            {
                Update(item);
            }
        }

        public void Delete(TEntity item)
        {
            databaseContext.Entry(item).State = EntityState.Deleted;
        }

        public void Delete(IEnumerable<TEntity> items)
        {
            try
            {
                for (int i = 0; i < items.Count(); i++)
                {
                    Delete(items.ElementAt(i));
                    --i;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual List<TEntity> ListAll()
        {
            return databaseContext.Set<TEntity>().ToList();
        }

        public virtual async Task<List<TEntity>> ListAllAsync()
        {
            return await databaseContext.Set<TEntity>().AsNoTracking().ToListAsync();
        }

        public virtual async Task<List<TEntity>> ListAllWithTrackingAsync()
        {
            return await databaseContext.Set<TEntity>().ToListAsync();
        }

        public List<TEntity> ListFiltered(Expression<Func<TEntity, bool>> filter)
        {
            return databaseContext.Set<TEntity>().Where(filter).ToList();
        }

        public async Task<List<TEntity>> ListFilteredAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await databaseContext.Set<TEntity>().AsNoTracking().Where(filter).ToListAsync();
        }

        public async Task<List<TEntity>> ListFilteredWithTrackingAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await databaseContext.Set<TEntity>().Where(filter).ToListAsync();
        }

        public async Task<List<TEntity>> ListPagedAsync<S>(int pageIndex, int pageSize, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true)
        {
            if (pageIndex < 0)
                throw new ArgumentOutOfRangeException("El índice de página no puede ser menor a cero.");

            if (pageSize <= 0)
                throw new ArgumentOutOfRangeException("El tamaño de la página debe ser mayor a cero.");

            if (orderByExpression == (Expression<Func<TEntity, S>>)null)
                throw new ArgumentNullException("La expresión de ordenamiento no puede ser nula.");

            var source = databaseContext.Set<TEntity>().AsNoTracking();

            var query = source;

            return ascending ? await query.OrderBy(orderByExpression)
                                          .Skip(pageIndex * pageSize)
                                          .Take(pageSize)
                                          .ToListAsync()
                             :
                               await query.OrderByDescending(orderByExpression)
                                          .Skip(pageIndex * pageSize)
                                          .Take(pageSize)
                                          .ToListAsync();
        }

        public bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)
                    return databaseContext.Set<TEntity>().Any();

                return databaseContext.Set<TEntity>().Any(filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)
                    return await databaseContext.Set<TEntity>().AnyAsync();

                return await databaseContext.Set<TEntity>().AnyAsync(filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TEntity Find(params object[] keyValues)
        {
            try
            {
                return databaseContext.Set<TEntity>().Find(keyValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> FindAsync(params object[] keyValues)
        {
            try
            {
                return await databaseContext.Set<TEntity>().FindAsync(keyValues);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)
                    return databaseContext.Set<TEntity>().AsNoTracking().FirstOrDefault();

                return databaseContext.Set<TEntity>().AsNoTracking().FirstOrDefault(filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TEntity FirstOrDefaultWithTracking(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)
                    return databaseContext.Set<TEntity>().FirstOrDefault();

                return databaseContext.Set<TEntity>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)

                    return await databaseContext.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync();

                return await databaseContext.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> FirstOrDefaulWithTrackingAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                if (filter == null)

                    return await databaseContext.Set<TEntity>().FirstOrDefaultAsync();

                return await databaseContext.Set<TEntity>().FirstOrDefaultAsync(filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<TEntity> QueryableAll()
        {
            return databaseContext.Set<TEntity>().AsNoTracking().AsQueryable();
        }

        public IQueryable<TEntity> QueryableAllWithTracking()
        {
            return databaseContext.Set<TEntity>().AsQueryable();
        }

        public IQueryable<TEntity> QueryableFiltered(Expression<Func<TEntity, bool>> filter)
        {
            return databaseContext.Set<TEntity>().AsNoTracking().Where(filter).AsQueryable();
        }

        public IQueryable<TEntity> QueryableFilteredWithTracking(Expression<Func<TEntity, bool>> filter)
        {
            return databaseContext.Set<TEntity>().Where(filter).AsQueryable();
        }
    }
}
