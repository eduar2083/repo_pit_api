﻿using Domain.Core.Contracts.UnitOfWork;
using Infraestructure.Data.Core.Contracts;
using System;
using System.Threading.Tasks;

namespace Infraestructure.Data.Core.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDatabaseContext databaseContext;

        public UnitOfWork(IDatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        #region Métodos Públicos
        public int Commit()
        {
            return databaseContext.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await databaseContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            try
            {
                if (databaseContext != null)
                {
                    databaseContext.Dispose();
                    databaseContext = null;
                    GC.SuppressFinalize(this);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
