﻿using Application.Core.Contracs;
using Domain.Core.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Core.Services
{
    public class GenericAppService<TEntity> : IGenericAppService<TEntity> where TEntity : class
    {
        private readonly IGenericService<TEntity> service;

        public GenericAppService(IGenericService<TEntity> service)
        {
            this.service = service;
        }

        public TEntity Add(TEntity item)
        {
            return service.Add(item);
        }

        public async Task<int> AddAsync(TEntity item)
        {
            return await service.AddAsync(item);
        }

        public IEnumerable<TEntity> Add(IEnumerable<TEntity> items)
        {
            return service.Add(items);
        }

        public async Task<int> AddAsync(IEnumerable<TEntity> items)
        {
            return await service.AddAsync(items);
        }

        public int Update(TEntity item)
        {
            return service.Update(item);
        }

        public async Task<int> UpdateAsync(TEntity item)
        {
            return await service.UpdateAsync(item);
        }

        public int Update(IEnumerable<TEntity> items)
        {
            return service.Update(items);
        }

        public async Task<int> UpdateAsync(IEnumerable<TEntity> items)
        {
            return await service.UpdateAsync(items);
        }

        public int Delete(TEntity item)
        {
            return service.Delete(item);
        }

        public async Task<int> DeleteAsync(TEntity item)
        {
            return await service.DeleteAsync(item);
        }

        public int Delete(IEnumerable<TEntity> items)
        {
            return service.Delete(items);
        }

        public async Task<int> DeleteAsync(IEnumerable<TEntity> items)
        {
            return await service.DeleteAsync(items);
        }

        public List<TEntity> ListAll()
        {
            return service.ListAll();
        }

        public async Task<List<TEntity>> ListAllAsync()
        {
            return await service.ListAllAsync();
        }

        public async Task<List<TEntity>> ListAllWithTrackingAsync()
        {
            return await service.ListAllWithTrackingAsync();
        }

        public List<TEntity> ListFiltered(Expression<Func<TEntity, bool>> filter)
        {
            return service.ListFiltered(filter);
        }

        public async Task<List<TEntity>> ListFilteredAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await service.ListFilteredAsync(filter);
        }

        public async Task<List<TEntity>> ListFilteredWithTrackingAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await ListFilteredWithTrackingAsync(filter);
        }

        public async Task<List<TEntity>> ListPagedAsync<S>(int pageIndex, int pageSize, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true)
        {
            return await service.ListPagedAsync(pageIndex, pageSize, orderByExpression, ascending);
        }

        public bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            return service.Any(filter);
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await service.AnyAsync(filter);
        }

        public TEntity Find(params object[] keyValues)
        {
            return service.Find(keyValues);
        }

        public async Task<TEntity> FindAsync(params object[] keyValues)
        {
            return await service.FindAsync(keyValues);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            return service.FirstOrDefault(filter);
        }

        public TEntity FirstOrDefaultWithTracking(Expression<Func<TEntity, bool>> filter = null)
        {
            return service.FirstOrDefaultWithTracking();
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await service.FirstOrDefaultAsync(filter);
        }

        public async Task<TEntity> FirstOrDefaulWithTrackingAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return await service.FirstOrDefaulWithTrackingAsync(filter);
        }
    }
}
