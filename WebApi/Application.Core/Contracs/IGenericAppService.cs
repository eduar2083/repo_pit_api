﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Core.Contracs
{
    public interface IGenericAppService<TEntity> where TEntity : class
    {
        TEntity Add(TEntity item);
        Task<int> AddAsync(TEntity item);

        IEnumerable<TEntity> Add(IEnumerable<TEntity> items);
        Task<int> AddAsync(IEnumerable<TEntity> items);

        int Update(TEntity item);
        Task<int> UpdateAsync(TEntity item);

        int Update(IEnumerable<TEntity> items);
        Task<int> UpdateAsync(IEnumerable<TEntity> items);

        int Delete(TEntity item);
        Task<int> DeleteAsync(TEntity item);

        int Delete(IEnumerable<TEntity> items);
        Task<int> DeleteAsync(IEnumerable<TEntity> items);

        List<TEntity> ListAll();
        Task<List<TEntity>> ListAllAsync();
        Task<List<TEntity>> ListAllWithTrackingAsync();

        List<TEntity> ListFiltered(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListFilteredAsync(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListFilteredWithTrackingAsync(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> ListPagedAsync<S>(int pageIndex, int pageSize, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true);

        bool Any(Expression<Func<TEntity, bool>> filter = null);
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null);

        TEntity Find(params object[] keyValues);
        Task<TEntity> FindAsync(params object[] keyValues);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null);
        TEntity FirstOrDefaultWithTracking(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> FirstOrDefaulWithTrackingAsync(Expression<Func<TEntity, bool>> filter = null);
    }
}
