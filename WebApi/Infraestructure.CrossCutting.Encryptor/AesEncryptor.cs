﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Infraestructure.CrossCutting.Encryptor
{
    internal sealed class AesEncryptor
    {
        // Cada uno puede modificar los números de clave y semilla a su gusto, pero recordemos que el algoritmo AES necesita 32 y 16 número como clave y semilla respectivamente
        private static readonly byte[] rgbClave = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64 };
        private static readonly byte[] rgbSemilla = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31 };

        #region Para encriptar
        public static string Encriptar(string strOriginal)
        {
            string resultado;

            byte[] data;

            try
            {
                // Encriptar la cadena mediante una función especializada
                data = EncryptStringToBytes(strOriginal, rgbClave, rgbSemilla);

                resultado = TranformarByteArrayEnStringHexa(data);

                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] EncryptStringToBytes(string strOriginal, byte[] rgbClave, byte[] rgbSemilla)
        {
            try
            {
                // Check arguments.
                if (strOriginal == null || strOriginal.Length <= 0)
                    throw new ArgumentNullException("strOriginal");
                if (rgbClave == null || rgbClave.Length <= 0)
                    throw new ArgumentNullException("rgbClave");
                if (rgbSemilla == null || rgbSemilla.Length <= 0)
                    throw new ArgumentNullException("rgbSemilla");

                byte[] resultado;

                // Crear un stream en memoria a partir del array de bytes con los datos encriptados
                using (MemoryStream mStream = new MemoryStream())
                {
                    // Crear el proveedor
                    using (Aes aes = Aes.Create())
                    {
                        // Crear el transformador en base a la clave y semilla
                        using (ICryptoTransform transform = aes.CreateEncryptor(rgbClave, rgbSemilla))
                        {
                            // En base al MemoryStream, crear un CryptoStream
                            using (CryptoStream cStream = new CryptoStream(mStream, transform, CryptoStreamMode.Write))
                            {
                                using (StreamWriter sWriter = new StreamWriter(cStream))
                                {
                                    sWriter.Write(strOriginal);
                                }

                                resultado = mStream.ToArray();
                            }
                        }
                    }
                }

                // Devolver el array de bytes
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string TranformarByteArrayEnStringHexa(byte[] data)
        {
            string strByte;
            string resultado = string.Empty;
            int n = -1;

            foreach (var b in data)
            {
                n++;
                strByte = string.Format("{0:X}", b);
                if (strByte.Length == 1)
                    strByte = "0" + strByte;
                resultado += strByte;
            }

            return resultado;
        }
        #endregion

        #region Para desencriptar
        public static string Desencriptar(string strOriginalHexa)
        {
            string resultado;
            byte[] data;

            try
            {
                data = TranformarStringHexaEnByteArray(strOriginalHexa);

                // Desencriptar el array de bytes mediante una función especializada
                resultado = DecryptStringFromBytes(data, rgbClave, rgbSemilla);

                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string DecryptStringFromBytes(byte[] datosEncriptados, byte[] rgbClave, byte[] rgbSemilla)
        {
            try
            {
                // Check arguments.
                if (datosEncriptados == null || datosEncriptados.Length <= 0)
                    throw new ArgumentNullException("datosEncriptados");
                if (rgbClave == null || rgbClave.Length <= 0)
                    throw new ArgumentNullException("rgbClave");
                if (rgbSemilla == null || rgbSemilla.Length <= 0)
                    throw new ArgumentNullException("rgbSemilla");

                string resultado;

                // Crear un stream en memoria
                using (MemoryStream mStream = new MemoryStream(datosEncriptados))
                {
                    // Crear el proveedor
                    using (AesManaged des = new AesManaged())
                    {
                        // Crear el transformador en base a la clave y semlla
                        using (ICryptoTransform transform = des.CreateDecryptor(rgbClave, rgbSemilla))
                        {
                            // En base al MemoryStream, crear un CryptoStream teniendo en cuenta la clave y semilla
                            using (CryptoStream cStream = new CryptoStream(mStream, transform, CryptoStreamMode.Read))
                            {
                                using (StreamReader sReader = new StreamReader(cStream))
                                {
                                    resultado = sReader.ReadToEnd();
                                }
                            }
                        }
                    }
                }

                // Devolver el string
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] TranformarStringHexaEnByteArray(string strEncriptadoHexa)
        {
            int nLength;
            int modulo;
            int contador;
            byte[] resultado;
            string strParcial;

            nLength = strEncriptadoHexa.Length;
            modulo = nLength % 2;
            resultado = new byte[nLength / 2];

            contador = -1;

            for (int n = 0; n < nLength; n += 2)
            {
                contador++;
                strParcial = strEncriptadoHexa.Substring(n, 2);
                resultado[contador] = Convert.ToByte(strParcial, 16);
            }

            return resultado;
        }
        #endregion
    }
}
