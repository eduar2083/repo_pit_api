﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Infraestructure.CrossCutting.Encryptor
{
    internal sealed class TripleDesEncryptor
    {
        // Cada uno puede modificar los números de clave y semilla a su gusto, pero recordemos que el algoritmo TripleDES necesita 8 números como clave
        private static readonly byte[] rgbClave = { 2, 4, 6, 8, 10, 12, 14, 16 };
        private static readonly byte[] rgbSemilla = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31 };

        #region Para encriptar
        public static string Encriptar(string strOriginal)
        {
            string resultado;

            byte[] data;

            try
            {
                // Encriptar la cadena mediante una función especializada
                data = EncryptStringToBytes(strOriginal, rgbClave, rgbSemilla);

                resultado = TranformarByteArrayEnStringHexa(data);

                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] EncryptStringToBytes(string strOriginal, byte[] rgbClave, byte[] rgbSemilla)
        {
            try
            {
                // Check arguments.
                if (strOriginal == null || strOriginal.Length <= 0)
                    throw new ArgumentNullException("strOriginal");
                if (rgbClave == null || rgbClave.Length <= 0)
                    throw new ArgumentNullException("rgbClave");
                if (rgbSemilla == null || rgbSemilla.Length <= 0)
                    throw new ArgumentNullException("rgbSemilla");

                byte[] resultado;

                // Crear un stream en memoria a partir del array de bytes con los datos encriptados
                using (MemoryStream mStream = new MemoryStream())
                {
                    // Crear el proveedor
                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        // Crear el transformador en base a la clave y semilla
                        using (ICryptoTransform transform = des.CreateEncryptor(rgbClave, rgbSemilla))
                        {
                            // En base al MemoryStream, crear un CryptoStream
                            using (CryptoStream cStream = new CryptoStream(mStream, transform, CryptoStreamMode.Write))
                            {
                                // Convertir la cadena en array de bytes
                                byte[] toEncrypt = new ASCIIEncoding().GetBytes(strOriginal);

                                // Convertir el array de bytes en el stream del encriptador y forzar su escritura física en memoria sin emplear cachés (vía Flush)
                                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                                cStream.FlushFinalBlock();

                                // Sacar un MemoryStream a partir del array de bytes. De ahí salen los datos encriptados
                                resultado = mStream.ToArray();
                            }
                        }
                    }
                }

                // Devolver el array de bytes
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string TranformarByteArrayEnStringHexa(byte[] data)
        {
            string strByte;
            string resultado = string.Empty;
            int n = -1;

            foreach (var b in data)
            {
                n++;
                strByte = string.Format("{0:X}", b);
                if (strByte.Length == 1)
                    strByte = "0" + strByte;
                resultado += strByte;
            }

            return resultado;
        }

        #endregion

        #region Para desencriptar
        public static string Desencriptar(string strOriginalHexa)
        {
            string resultado;
            byte[] data;

            try
            {
                data = TranformarStringHexaEnByteArray(strOriginalHexa);

                // Desencriptar el array de bytes mediante una función especializada
                resultado = DecryptStringFromBytes(data, rgbClave, rgbSemilla);

                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string DecryptStringFromBytes(byte[] datosEncriptados, byte[] rgbClave, byte[] rgbSemilla)
        {
            try
            {
                // Check arguments.
                if (datosEncriptados == null || datosEncriptados.Length <= 0)
                    throw new ArgumentNullException("datosEncriptados");
                if (rgbClave == null || rgbClave.Length <= 0)
                    throw new ArgumentNullException("rgbClave");
                if (rgbSemilla == null || rgbSemilla.Length <= 0)
                    throw new ArgumentNullException("rgbSemilla");

                string resultado;

                // Crear un stream en memoria
                using (MemoryStream mStream = new MemoryStream(datosEncriptados))
                {
                    // Crear el proveedor
                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        // Crear el transformador en base a la clave y semlla
                        using (ICryptoTransform transform = des.CreateDecryptor(rgbClave, rgbSemilla))
                        {
                            // En base al MemoryStream, crear un CryptoStream teniendo en cuenta la clave y semilla
                            using (CryptoStream cStream = new CryptoStream(mStream, transform, CryptoStreamMode.Read))
                            {
                                // Preparar un buffer para almacenar los datos desencriptados
                                byte[] fromEncrypt = new byte[datosEncriptados.Length];

                                // Sacar los datos del CryptoStream y colocarlos en el buffer
                                cStream.Read(fromEncrypt, 0, fromEncrypt.Length);

                                // Pasar de array de bytes a string
                                resultado = new ASCIIEncoding().GetString(fromEncrypt);

                                if (resultado.Length > 0)
                                    resultado = resultado.Trim(new char[] { '\0', ' ' });
                            }
                        }
                    }
                }

                // Devolver el string
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] TranformarStringHexaEnByteArray(string strEncriptadoHexa)
        {
            int nLength;
            int modulo;
            int contador;
            byte[] resultado;
            string strParcial;

            nLength = strEncriptadoHexa.Length;
            modulo = nLength % 2;
            resultado = new byte[nLength / 2];

            contador = -1;

            for (int n = 0; n < nLength; n += 2)
            {
                contador++;
                strParcial = strEncriptadoHexa.Substring(n, 2);
                resultado[contador] = Convert.ToByte(strParcial, 16);
            }

            return resultado;
        }
        #endregion
    }
}
