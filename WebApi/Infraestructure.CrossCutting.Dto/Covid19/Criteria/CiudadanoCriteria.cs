﻿namespace Infraestructure.CrossCutting.Dto.Covid19.Criteria
{
    public class CiudadanoCriteria
    {
        public string Nombre { get; set; }

        public string NroDocumentoIdentidad { get; set; }

        public int? EstadoId { get; set; }
    }
}
