﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class SituacionRiesgoDto
    {
        public int SituacionRiesgoId { get; set; }

        public int CiudadanoId { get; set; }
    }
}
