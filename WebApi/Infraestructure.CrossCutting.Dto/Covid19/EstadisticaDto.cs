﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class EstadisticaDto
    {
        public int Nuevos { get; set; }

        public int Confirmados { get; set; }

        public int Recuperados { get; set; }

        public int Fallecidos { get; set; }
    }
}
