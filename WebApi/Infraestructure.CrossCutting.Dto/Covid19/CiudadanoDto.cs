﻿using System;

namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class CiudadanoDto
    {
        public int CiudadanoId { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Nombre { get; set; }
        public bool Sexo { get; set; }
        public int NacionalidadId { get; set; }
        public int TipoDocumentoIdentidadId { get; set; }
        public string NroDocumentoIdentidad { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public DateTime FechaNacimiento { get; set; }

        public string TipoDocumentoIdentidadDesc { get; set; }
        public string EstadoDesc { get; set; }
    }
}
