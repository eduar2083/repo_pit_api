﻿using System;

namespace Infraestructure.CrossCutting.Dto.Base
{
    public class DtoBase
    {
        public string UsuarioCreacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public DateTime? FechaActualizacion { get; set; }

        // Información sensible encriptada (campo ignorado)
        public string Encrypt { get; set; }
    }
}
