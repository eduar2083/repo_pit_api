﻿using System.ComponentModel.DataAnnotations;

namespace Infraestructure.CrossCutting.Dto.Common
{
    public class PaisDto
    {
        public int PaisId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 2, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string CodigoISO { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 100, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }
    }
}
