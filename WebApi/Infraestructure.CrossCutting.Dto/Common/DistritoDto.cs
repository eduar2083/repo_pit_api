﻿using System.ComponentModel.DataAnnotations;

namespace Infraestructure.CrossCutting.Dto.Common
{
    public class DistritoDto
    {
        public int DistritoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 6, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Ubigeo { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 100, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }
    }
}
