﻿using System;

namespace Infraestructure.CrossCutting.Dto.Util
{
    public class TokenDto
    {
        public string Token { get; set; }
        public DateTime Expires { get; set; }
    }
}
