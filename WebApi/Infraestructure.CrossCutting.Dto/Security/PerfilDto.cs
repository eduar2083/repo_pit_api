﻿namespace Infraestructure.CrossCutting.Dto.Security
{
    public class PerfilDto
    {
        public int PerfilId { get; set; }

        public string Nombre { get; set; }
    }
}
