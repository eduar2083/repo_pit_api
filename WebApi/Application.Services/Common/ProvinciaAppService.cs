﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Common;
using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Services.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Common
{
    public class ProvinciaAppService : GenericAppService<Provincia>, IProvinciaAppService
    {
        private readonly IProvinciaService provinciaService;

        public ProvinciaAppService(IProvinciaService provinciaService) : base(provinciaService)
        {
            this.provinciaService = provinciaService;
        }

        public async Task<List<Provincia>> ListarPorDepartamentoAsync(int DepartamentoId)
        {
            return await provinciaService.ListarPorDepartamentoAsync(DepartamentoId);
        }
    }
}
