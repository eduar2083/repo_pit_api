﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Common;
using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Services.Common;

namespace Application.Services.Common
{
    public class TipoDocumentoIdentidadAppService : GenericAppService<TipoDocumentoIdentidad>, ITipoDocumentoIdentidadAppService
    {
        public TipoDocumentoIdentidadAppService(ITipoDocumentoIdentidadService tipoDocumentoIdentidadService) : base(tipoDocumentoIdentidadService)
        {
        }
    }
}
