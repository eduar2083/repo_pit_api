﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Covid19
{
    public class CiudadanoAppService : GenericAppService<Ciudadano>, ICiudadanoAppService
    {
        private readonly ICiudadanoService ciudadanoService;

        public CiudadanoAppService(ICiudadanoService ciudadanoService) : base(ciudadanoService)
        {
            this.ciudadanoService = ciudadanoService;
        }

        public Task<int> AgregarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo)
        {
            return ciudadanoService.AgregarAsync(ciudadano, contactos, direcciones, sintomas, situacionesRiesgo);
        }

        public async Task<int> ActualizarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo)
        {
            return await ciudadanoService.ActualizarAsync(ciudadano, contactos, direcciones, sintomas, situacionesRiesgo);
        }

        public async Task<int> EliminarAsync(Ciudadano ciudadano)
        {
            return await ciudadanoService.EliminarAsync(ciudadano);
        }

        public async Task<Ciudadano> ObtenerConDetalleAsync(int CiudadanoId)
        {
            return await ciudadanoService.ObtenerConDetalleAsync(CiudadanoId);
        }

        public async Task<List<Ciudadano>> FiltrarAsync(CiudadanoCriteria c)
        {
            return await ciudadanoService.FiltrarAsync(c);
        }
    }
}
