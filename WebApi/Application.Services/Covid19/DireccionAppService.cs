﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Covid19
{
    public class DireccionAppService : GenericAppService<Direccion>, IDireccionAppService
    {
        private readonly IDireccionService direccionService;

        public DireccionAppService(IDireccionService direccionService) : base(direccionService)
        {
            this.direccionService = direccionService;
        }

        public Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId)
        {
            return direccionService.ListarPorCiudadanoAsync(CiudadanoId);
        }

        public Task<List<Direccion>> ConfirmadosAsync(Point ubicacion)
        {
            return direccionService.ConfirmadosAsync(ubicacion);
        }
    }
}
