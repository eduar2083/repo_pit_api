﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Threading.Tasks;

namespace Application.Services.Covid19
{
    public class EstadisticaAppService : GenericAppService<Ciudadano>, IEstadisticaAppService
    {
        private readonly IEstadisticaService estadisticaService;

        public EstadisticaAppService(IEstadisticaService estadisticaService) : base(estadisticaService)
        {
            this.estadisticaService = estadisticaService;
        }

        public async Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c)
        {
            return await estadisticaService.FiltrarAsync(c);
        }
    }
}
