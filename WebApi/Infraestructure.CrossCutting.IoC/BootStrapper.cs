﻿using Application.Core.Contracs;
using Application.Core.Services;
using Application.MainModule.Contracts.Services.Common;
using Application.MainModule.Contracts.Services.Covid19;
using Application.MainModule.Contracts.Services.Security;
using Application.Services.Common;
using Application.Services.Covid19;
using Application.Services.Security;
using Domain.Core.Contracts.Repositories;
using Domain.Core.Contracts.Services;
using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Security;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Common;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Repositories.Security;
using Domain.MainModule.Contracts.Services.Common;
using Domain.MainModule.Contracts.Services.Covid19;
using Domain.MainModule.Contracts.Services.Security;
using Domain.MainModule.Contracts.UnitOfWork;
using Domain.MainModule.Services.Common;
using Domain.MainModule.Services.Covid19;
using Domain.MainModule.Services.Security;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Infraestructure.Data.Core.UnitOfWork;
using Infraestructure.Data.MainModule.Context;
using Infraestructure.Data.MainModule.Repositories.Common;
using Infraestructure.Data.MainModule.Repositories.Covid19;
using Infraestructure.Data.MainModule.Repositories.Security;
using Infraestructure.Data.MainModule.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Infraestructure.CrossCutting.IoC
{
	public class BootStrapper
    {
        public static void RegisterServices(IServiceCollection service, IConfiguration Configuration)
        {
			try
			{
				RegisterGenericServices(service, Configuration);
				RegisterMainModuleServices(service, Configuration);
			}
			catch (Exception ex)
			{
				throw ex;
			}
        }

		private static void RegisterGenericServices(IServiceCollection service, IConfiguration Configuration)
		{
			service.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Cnx01"), x => x.UseNetTopologySuite()));
			service.AddScoped<IDatabaseContext, DataContext>();

			service.AddScoped<IUnitOfWork, UnitOfWork>();
			service.AddScoped<IUnitOfWorkMainModule, UnitOfWorkMainModule>();

			service.AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>();
			service.AddScoped<IUnitOfWorkMainModuleFactory, UnitOfWorkMainModuleFactory>();

			service.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
			service.AddScoped(typeof(IGenericService<>), typeof(GenericService<>));
			service.AddScoped(typeof(IGenericAppService<>), typeof(GenericAppService<>));

			service.AddScoped<IPasswordHasher<Usuario>, PasswordHasher<Usuario>>();
		}

		private static void RegisterMainModuleServices(IServiceCollection service, IConfiguration Configuration)
		{
			#region Repositories
			#region Security
			service.AddScoped<IPerfilRepository, PerfilRepository>();
			service.AddScoped<IUsuarioRepository, UsuarioRepository>();
			#endregion

			#region Common
			service.AddScoped<IPaisRepository, PaisRepository>();
			service.AddScoped<ITipoDocumentoIdentidadRepository, TipoDocumentoIdentidadRepository>();
			service.AddScoped<IDepartamentoRepository, DepartamentoRepository>();
			service.AddScoped<IProvinciaRepository, ProvinciaRepository>();
			service.AddScoped<IDistritoRepository, DistritoRepository>();
			#endregion

			#region Covid19
			service.AddScoped<IEstadoRepository, EstadoRepository>();
			service.AddScoped<ICiudadanoRepository, CiudadanoRepository>();
			service.AddScoped<IContactoRepository, ContactoRepository>();
			service.AddScoped<IDireccionRepository, DireccionRepository>();
			service.AddScoped<ISintomaRepository, SintomaRepository>();
			service.AddScoped<ISituacionRiesgoRepository, SituacionRiesgoRepository>();
			service.AddScoped<IEstadisticaRepository, EstadisticaRepository>();
			#endregion
			#endregion

			#region Domain Services
			#region Security
			service.AddScoped<IPerfilService, PerfilService>();
			service.AddScoped<IUsuarioService, UsuarioService>();
			#endregion

			#region Common
			service.AddScoped<IPaisService, PaisService>();
			service.AddScoped<ITipoDocumentoIdentidadService, TipoDocumentoIdentidadService>();
			service.AddScoped<IDepartamentoService, DepartamentoService>();
			service.AddScoped<IProvinciaService, ProvinciaService>();
			service.AddScoped<IDistritoService, DistritoService>();
			#endregion

			#region Covid19
			service.AddScoped<IEstadoService, EstadoService>();
			service.AddScoped<ICiudadanoService, CiudadanoService>();
			service.AddScoped<IContactoService, ContactoService>();
			service.AddScoped<IDireccionService, DireccionService>();
			service.AddScoped<ISintomaService, SintomaService>();
			service.AddScoped<ISituacionRiesgoService, SituacionRiesgoService>();
			service.AddScoped<IEstadisticaService, EstadisticaService>();
			#endregion
			#endregion

			#region Application Services
			#region Security
			service.AddScoped<IPerfilAppService, PerfilAppService>();
			service.AddScoped<IUsuarioAppService, UsuarioAppService>();
			#endregion

			#region Common
			service.AddScoped<IPaisAppService, PaisAppService>();
			service.AddScoped<ITipoDocumentoIdentidadAppService, TipoDocumentoIdentidadAppService>();
			service.AddScoped<IDepartamentoAppService, DepartamentoAppService>();
			service.AddScoped<IProvinciaAppService, ProvinciaAppService>();
			service.AddScoped<IDistritoAppService, DistritoAppService>();
			#endregion

			#region Covid19
			service.AddScoped<IEstadoAppService, EstadoAppService>();
			service.AddScoped<ICiudadanoAppService, CiudadanoAppService>();
			service.AddScoped<IContactoAppService, ContactoAppService>();
			service.AddScoped<IDireccionAppService, DireccionAppService>();
			service.AddScoped<ISintomaAppService, SintomaAppService>();
			service.AddScoped<ISituacionRiesgoAppService, SituacionRiesgoAppService>();
			service.AddScoped<IEstadisticaAppService, EstadisticaAppService>();
			#endregion
			#endregion
		}
    }
}
