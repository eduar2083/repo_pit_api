﻿using AutoMapper;
using Infraestructure.CrossCutting.IoC;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using Serilog;
using System;
using System.Text;
using WebApi.AutoMapper.Profiles;
using WebApi.Services;

namespace WebApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureLogging(this IConfiguration Configuration)
        {
            bool enabled = Configuration.GetValue<bool>("Serilog:Enabled");
            Log.Logger = new LoggerConfiguration()
                            .Filter.ByExcluding(_ => !enabled)
                            .ReadFrom.Configuration(Configuration)
                            .CreateLogger();
        }

        public static void ConfigureCors(this IServiceCollection services, IConfiguration Configuration, string policyName)
        {
            var allowedOrigins = Configuration.GetSection("CORS-Settings:Allow-Origins").Get<string[]>();
            var allowedMethods = Configuration.GetSection("CORS-Settings:Allow-Methods").Get<string[]>();

            services.AddCors(options => {
                options.AddPolicy(policyName, buillder =>
                {
                    buillder.WithOrigins(allowedOrigins)
                            .AllowAnyHeader()
                            .WithMethods(allowedMethods);
                });
            });
        }

        public static void ConfigureJwt(this IServiceCollection services, IConfiguration Configuration)
        {
            var jwtSettings = Configuration.GetSection("JwtSettings");
            var secretKey = jwtSettings.GetValue<string>("SecretKey");
            var minutes = jwtSettings.GetValue<int>("MinutesToExpiration");
            var issuer = jwtSettings.GetValue<string>("Issuer");
            var audience = jwtSettings.GetValue<string>("Audience");

            var key = Encoding.UTF8.GetBytes(secretKey);

            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidIssuer = issuer,
                    ValidateAudience = true,
                    ValidAudience = audience,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero   // Esta propiedad permite indicarle al algoritmo que ajuste la expiración a un cierta cantidad de tiempo
                };
            });
        }

        public static void ConfigureNetTopologySuite(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient((provider) => NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326));
        }

        public static void ConfigureDependencies(this IServiceCollection services, IConfiguration Configuration)
        {
            BootStrapper.RegisterServices(services, Configuration);

            services.AddSingleton<TokenService>();
        }

        public static void ConfigureAutoMapperProfile(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new CommonProfile());
                cfg.AddProfile(new CovidProfile(provider.GetService<GeometryFactory>()));
                cfg.AddProfile(new SecurityProfile());
            }).CreateMapper());
        }
    }
}
