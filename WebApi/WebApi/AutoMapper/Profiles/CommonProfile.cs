﻿using AutoMapper;
using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Dto.Common;

namespace WebApi.AutoMapper.Profiles
{
    public class CommonProfile : Profile
    {
        public CommonProfile()
        {
            CreateMap<Pais, PaisDto>();
            CreateMap<TipoDocumentoIdentidad, TipoDocumentoIdentidadDto>();
            CreateMap<Departamento, DepartamentoDto>();
            CreateMap<Provincia, ProvinciaDto>();
            CreateMap<Distrito, DistritoDto>();
        }
    }
}
