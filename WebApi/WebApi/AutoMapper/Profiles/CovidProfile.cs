﻿using AutoMapper;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using NetTopologySuite.Geometries;
using WebApi.ViewModels.Covid19;

namespace WebApi.AutoMapper.Profiles
{
    public class CovidProfile : Profile
    {
        public CovidProfile(GeometryFactory geometryFactory)
        {
            #region Post, Put
            CreateMap<EstadoVM, Estado>();

            CreateMap<CiudadanoVM, Ciudadano>();

            CreateMap<ContactoVM, Contacto>();

            CreateMap<DireccionVM, Direccion>()
                .ForMember(t => t.Ubicacion, opt => opt.MapFrom(t => geometryFactory.CreatePoint(new Coordinate(t.Longitud, t.Latitud))));

            CreateMap<SintomaVM, Sintoma>();

            CreateMap<SituacionRiesgoVM, SituacionRiesgo>();
            #endregion

            #region Get
            CreateMap<Estado, EstadoDto>();

            CreateMap<Ciudadano, CiudadanoDto>()
                .ForMember(t => t.TipoDocumentoIdentidadDesc, opt => opt.MapFrom(t => t.TipoDocumentoIdentidad.DescripcionCorta))
                .ForMember(t => t.EstadoDesc, opt => opt.MapFrom(t => t.Estado.Nombre));

            CreateMap<Ciudadano, CiudadanoDetailDto>();

            CreateMap<Contacto, ContactoDto>();

            CreateMap<Direccion, DireccionDto>()
                .ForMember(t => t.Latitud, opt => opt.MapFrom(t => t.Ubicacion.Y))
                .ForMember(t => t.Longitud, opt => opt.MapFrom(t => t.Ubicacion.X))
                .ForMember(t => t.ProvinciaId, opt => opt.MapFrom(t => t.Distrito.Provincia.ProvinciaId))
                .ForMember(t => t.DepartamentoId, opt => opt.MapFrom(t => t.Distrito.Provincia.Departamento.DepartamentoId));

            CreateMap<Sintoma, SintomaDto>();

            CreateMap<SituacionRiesgo, SituacionRiesgoDto>();
            #endregion

        }
    }
}
