﻿using AutoMapper;
using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Dto.Security;
using WebApi.ViewModels.Security;

namespace WebApi.AutoMapper.Profiles
{
    public class SecurityProfile : Profile
    {
        public SecurityProfile()
        {
            #region Get
            CreateMap<Perfil, PerfilDto>();

            CreateMap<Usuario, UsuarioDto>()
                .ForMember(a => a.Perfil, b => b.MapFrom(c => c.Perfil.Nombre));

           
            #endregion

            #region Post, Put
            CreateMap<PerfilVM, Perfil>();

            CreateMap<UsuarioCreateVM, Usuario>();

            CreateMap<UsuarioChangePasswordVM, Usuario>();

            CreateMap<UsuarioLoginVM, Usuario>();
            #endregion
        }
    }
}
