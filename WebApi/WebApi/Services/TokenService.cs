﻿using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Dto.Util;
using Infraestructure.CrossCutting.Utilities.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace WebApi.Services
{
    public class TokenService
    {
        public IConfiguration Configuration { get; }

        public TokenService(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        public TokenDto Generar(Usuario usuario)
        {
            try
            {
                var jwtSettings = Configuration.GetSection("JwtSettings");
                var secretKey = jwtSettings.GetValue<string>("SecretKey");
                var minutes = jwtSettings.GetValue<int>("MinutesToExpiration");
                var issuer = jwtSettings.GetValue<string>("Issuer");
                var audience = jwtSettings.GetValue<string>("Audience");

                var now = DateTime.UtcNow;

                // Clave secreta
                var key = Encoding.UTF8.GetBytes(secretKey);

                // Datos
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),  // Identificador único del token incluso entre diferente proveedores de servicio
                    new Claim(JwtRegisteredClaimNames.Sub, usuario.Username),   // Identifica el objeto o usuario en nombre del cual fue emitido el JWT
                    new Claim(JwtRegisteredClaimNames.Iat, DateTimeParse.ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64), // Identifica la marca temporal en qué el JWT fue emitido
                    new Claim(JwtRegisteredClaimNames.Email, usuario.Email),

                    new Claim(ClaimTypes.Role, usuario.Perfil.Nombre)
                };

                // Credenciales
                var creds = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

                // Expiración
                DateTime expires = now.AddMinutes(minutes);

                var token = new JwtSecurityToken(
                    issuer: issuer, // Identifica el proveedor de identidad que emitió el JWT
                    audience: audience, // Identifica la audiencia o receptores para lo que el JWT fue emitido, normalmente el/los servidor/es de recursos (e.g. la API protegida). Cada servicio que recibe un JWT para su validación tiene que controlar la audiencia a la que el JWT está destinado. Si el proveedor del servicio no se encuentra presente en el campo aud, entonces el JWT tiene que ser rechazado
                    claims: claims,
                    notBefore: now, // Identifica la marca temporal en que el JWT comienza a ser válido. EL JWT no tiene que ser aceptado si el token es utilizando antes de este tiempo. 
                    expires: expires,   // Identifica la marca temporal luego de la cual el JWT no tiene que ser aceptado. 
                    signingCredentials: creds
                );

                // Crear una representación en cadena del JWT
                string r = new JwtSecurityTokenHandler().WriteToken(token);

                return new TokenDto
                {
                    Token = r,
                    Expires = expires
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
