﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class DireccionVM
    {
        public int DireccionId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int DistritoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public double Longitud { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public double Latitud { get; set; }

        public int CiudadanoId { get; set; }
    }
}
