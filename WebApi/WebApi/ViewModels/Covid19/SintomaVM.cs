﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class SintomaVM
    {
        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int SintomaId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int Ciudadanoid { get; set; }
    }
}
