﻿using Infraestructure.CrossCutting.Utilities.Enumerations;
using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class UsuarioCreateVM
    {
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }

        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "\'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(maximumLength: 20, MinimumLength = 6, ErrorMessage = "\'{0}\' debe tener entre {2} y {1} caracteres")]
        public string Username { get; set; }

        [Required(ErrorMessage = "\'{0}\' es requerido")]
        [MinLength(6, ErrorMessage ="\'{0}\' debe tener al menos {1} caracteres")]
        public string Password { get; set; }

        [Required(ErrorMessage = "\'{0}\' es requerido")]
        [Display(Name = "Perfil")]
        public int PerfilId { get; set; }

        public EstadoUsuarioEnum Estado { get; set; }
    }
}
