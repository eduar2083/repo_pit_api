﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class UsuarioLoginVM
    {
        [Required(ErrorMessage = "El campo \"{0}\" es obligatorio.")]
        [MaxLength(30, ErrorMessage = "\"{0}\" debe tener cómo máximo {1} caracteres.")]
        [Display(Name = "Usuario")]
        public string Username { get; set; }

        [Required(ErrorMessage = "El campo \"{0}\" es obligatorio.")]
        [MaxLength(100, ErrorMessage = "\"{0}\" debe tener cómo máximo {1} caracteres")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        //[Display(Name = "Recordarme")]
        //public bool RememberMe { get; set; }
    }
}
