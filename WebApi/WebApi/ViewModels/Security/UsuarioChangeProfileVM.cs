﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class UsuarioChangeProfileVM
    {
        public int UsuarioId { get; set; }

        [Required(ErrorMessage ="El campo \"{0}\" es requerido")]
        [Display(Name ="Perfil")]
        public int PerfilId { get; set; }
    }
}
