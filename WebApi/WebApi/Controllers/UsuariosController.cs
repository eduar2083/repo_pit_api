﻿using Application.MainModule.Contracts.Services.Security;
using AutoMapper;
using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Dto.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebApi.ViewModels.Security;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Administrador")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuarioAppService usuarioAppService;
        private readonly IMapper mapper;
        private readonly ILogger<UsuariosController> logger;

        public UsuariosController(IUsuarioAppService usuarioAppService,
                                  IMapper mapper,
                                  ILogger<UsuariosController> logger)
        {
            this.usuarioAppService = usuarioAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Usuarios
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<UsuarioDto>>> Get()
        {
            try
            {
                return mapper.Map<List<UsuarioDto>>(await usuarioAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Usuarios/detPerfil
        [HttpGet]
        [Route("detPerfil")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<UsuarioDto>>> GetWithPerfil()
        {
            try
            {
                return mapper.Map<List<UsuarioDto>>(await usuarioAppService.ListAllWithProfileAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UsuarioDto>> Get(int id)
        {
            try
            {
                var entidad = await usuarioAppService.FindAsync(id);

                if (entidad == null)
                {
                    return NotFound();
                }

                return mapper.Map<UsuarioDto>(entidad);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        // POST: api/Usuarios
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UsuarioDto>> Post(UsuarioCreateVM vm)
        {
            try
            {
                var entidad = mapper.Map<Usuario>(vm);

                await usuarioAppService.AddAsync(entidad);

                var dto = mapper.Map<UsuarioDto>(entidad);

                return CreatedAtAction(nameof(Get), new { id = dto.UsuarioId }, dto);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al crear el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Usuarios/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UsuarioDto>> Put(int id, UsuarioEditVM vm)
        {
            if (id != vm.UsuarioId)
            {
                logger.LogWarning($"No coincide el Id del parámetro con el modelo: {id}!={vm.UsuarioId}");

                return BadRequest();
            }

            try
            {
                var entidad = await usuarioAppService.FindAsync(id);
                if (entidad == null)
                    return NotFound();

                // Establecer cambios
                entidad.Nombre = vm.Nombre;
                entidad.ApellidoPaterno = vm.Apellidos;
                entidad.Email = vm.Email;

                int r = await usuarioAppService.UpdateAsync(entidad);
                if (r > 0)
                    return mapper.Map<UsuarioDto>(entidad);

                return NoContent();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (! await Exists(id))
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido eliminado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return NotFound();
                }
                else
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido modificado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return StatusCode(StatusCodes.Status304NotModified);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Usuarios/ChangePassword
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        [Route("ChangePassword")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Put(UsuarioChangePasswordVM vm)
        {
            try
            {
                //var entidad = await usuarioAppService.FirstOrDefaultAsync(t => t.UsuarioId == vm.UsuarioId);
                //if (entidad == null)
                //    return NotFound();

                var entidad = mapper.Map<Usuario>(vm);

                int r = await usuarioAppService.ChangePasswordAsync(entidad);
                if (r > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Usuarios/ChangeProfile
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        [Route("ChangeProfile")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Put(UsuarioChangeProfileVM vm)
        {
            try
            {
                var entidad = await usuarioAppService.FindAsync(vm.UsuarioId);
                if (entidad == null)
                    return NotFound();

                // Establecer cambios
                entidad.PerfilId = vm.PerfilId;

                int r = await usuarioAppService.UpdateAsync(entidad);
                if (r > 0)
                    return Ok();

                return BadRequest();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Usuario>> Delete(int id)
        {
            try
            {
                var entidad = await usuarioAppService.FindAsync(id);
                if (entidad == null)
                {
                    return NotFound();
                }

                await usuarioAppService.DeleteAsync(entidad);

                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al eliminar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private async Task<bool> Exists(int id)
        {
            return await usuarioAppService.AnyAsync(e => e.UsuarioId == id);
        }
    }
}
