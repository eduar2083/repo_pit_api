﻿using Application.MainModule.Contracts.Services.Common;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Administrador")]
    [ApiController]
    [Route("api/[Controller]")]
    public class ProvinciasController : ControllerBase
    {
        private readonly IProvinciaAppService provinciaAppService;
        private readonly IMapper mapper;
        private readonly ILogger<ProvinciasController> logger;

        public ProvinciasController(IProvinciaAppService provinciaAppService,
                                    IMapper mapper,
                                    ILogger<ProvinciasController> logger)
        {
            this.provinciaAppService = provinciaAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet("{DepartamentoId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<ProvinciaDto>>> Get(int DepartamentoId)
        {
            try
            {
                return mapper.Map<List<ProvinciaDto>>(await provinciaAppService.ListarPorDepartamentoAsync(DepartamentoId));
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
