﻿using Application.MainModule.Contracts.Services.Common;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize(Roles ="Administrador")]
    [ApiController]
    [Route("api/[Controller]")]
    public class DepartamentosController : ControllerBase
    {
        private readonly IDepartamentoAppService departamentoAppService;
        private readonly IMapper mapper;
        private readonly ILogger<DepartamentosController> logger;

        public DepartamentosController(IDepartamentoAppService departamentoAppService,
                                       IMapper mapper,
                                       ILogger<DepartamentosController> logger)
        {
            this.departamentoAppService = departamentoAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<DepartamentoDto>>> Get()
        {
            try
            {
                return mapper.Map<List<DepartamentoDto>>(await departamentoAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
