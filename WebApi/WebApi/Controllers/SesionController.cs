﻿using Application.MainModule.Contracts.Services.Security;
using AutoMapper;
using Domain.Core.Entities.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Mime;
using System.Threading.Tasks;
using WebApi.Services;
using WebApi.ViewModels.Security;

namespace WebApi.Controllers
{
    [AllowAnonymous]
    [Route("api/[Controller]")]
    [ApiController]
    public class SesionController : ControllerBase
    {
        private readonly IUsuarioAppService usuarioAppService;
        private readonly IMapper mapper;
        private readonly ILogger<SesionController> logger;
        private readonly TokenService tokenService;

        public SesionController(IUsuarioAppService usuarioAppService,
                                IMapper mapper,
                                ILogger<SesionController> logger,
                                TokenService tokenService)
        {
            this.usuarioAppService = usuarioAppService;
            this.mapper = mapper;
            this.logger = logger;
            this.tokenService = tokenService;
        }

        [Route("login")]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> PostLogin(UsuarioLoginVM vm)
        {
            try
            {
                var login = mapper.Map<Usuario>(vm);

                var validate = await usuarioAppService.CheckPasswordAsync(login);

                if (!validate.resultado)
                    return BadRequest("Usuario o Password incorrecto");

                var r = tokenService.Generar(validate.usuario);

                return Ok(r);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al generar al autenticar: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
