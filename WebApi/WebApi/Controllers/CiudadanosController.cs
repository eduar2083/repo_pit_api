﻿using Application.MainModule.Contracts.Services.Covid19;
using AutoMapper;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebApi.ViewModels.Covid19;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrador")]
    public class CiudadanosController : ControllerBase
    {
        private readonly ICiudadanoAppService ciudadanoAppService;
        private readonly IMapper mapper;
        private readonly ILogger<CiudadanosController> logger;

        public CiudadanosController(ICiudadanoAppService ciudadanoAppService,
                                    IMapper mapper,
                                    ILogger<CiudadanosController> logger)
        {
            this.ciudadanoAppService = ciudadanoAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Ciudadanos
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<CiudadanoDto>>> Get()
        {
            try
            {
                return mapper.Map<List<CiudadanoDto>>(await ciudadanoAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // Post: api/Ciudadanos/filtrar
        [HttpPost("filtrar")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<CiudadanoDto>>> PostFiltrar(CiudadanoCriteria criteria)
        {
            try
            {
                return mapper.Map<List<CiudadanoDto>>(await ciudadanoAppService.FiltrarAsync(criteria));
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Ciudadanos/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CiudadanoDto>> Get(int id)
        {
            try
            {
                var entidad = await ciudadanoAppService.FindAsync(id);

                if (entidad == null)
                {
                    return NotFound();
                }

                return mapper.Map<CiudadanoDto>(entidad);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Ciudadanos/5/detail
        [HttpGet("{id}/detail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CiudadanoDetailDto>> GetWithDetail(int id)
        {
            try
            {
                var entidad = await ciudadanoAppService.ObtenerConDetalleAsync(id);

                if (entidad == null)
                {
                    return NotFound();
                }

                return mapper.Map<CiudadanoDetailDto>(entidad);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/Ciudadanos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Ciudadano>> Post(CiudadanoTranVM vm)
        {
            try
            {
                var ciudadano = mapper.Map<Ciudadano>(vm.Ciudadano);
                var contactos = mapper.Map<List<Contacto>>(vm.Contactos);
                var direcciones = mapper.Map<List<Direccion>>(vm.Direcciones);
                var sintomas = mapper.Map<List<Sintoma>>(vm.Sintomas);
                var situacionesRiesgo = mapper.Map<List<SituacionRiesgo>>(vm.SituacionesRiesgo);

                await ciudadanoAppService.AgregarAsync(ciudadano, contactos, direcciones, sintomas, situacionesRiesgo);

                var dto = mapper.Map<CiudadanoDto>(ciudadano);

                return CreatedAtAction(nameof(Get), new { id = dto.CiudadanoId }, dto);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al crear el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Ciudadanos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CiudadanoDto>> Put(int id, CiudadanoVM vm)
        {
            if (id != vm.CiudadanoId)
            {
                logger.LogWarning($"No coincide el Id del parámetro con el modelo: {id}!={vm.CiudadanoId}");

                return BadRequest();
            }

            try
            {
                var entidad = await ciudadanoAppService.FindAsync(id);

                if (entidad == null)
                    return NotFound();

                // Establecer campos editables
                entidad.Nombre = vm.Nombre;
                entidad.ApellidoPaterno = vm.ApellidoPaterno;
                entidad.ApellidoMaterno = vm.ApellidoMaterno;
                entidad.Sexo = vm.Sexo;
                entidad.NacionalidadId = vm.NacionalidadId;
                entidad.TipoDocumentoIdentidadId = vm.TipoDocumentoIdentidadId;
                entidad.NroDocumentoIdentidad = vm.NroDocumentoIdentidad;
                entidad.Celular = vm.Celular;
                entidad.Correo = vm.Correo;
                entidad.FechaNacimiento = vm.FechaNacimiento;
                entidad.EstadoId = vm.EstadoId;

                int r = await ciudadanoAppService.UpdateAsync(entidad);
                if (r > 0)
                    return mapper.Map<CiudadanoDto>(entidad);

                return NoContent();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!await Exists(id))
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido eliminado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return NotFound();
                }
                else
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido modificado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return StatusCode(StatusCodes.Status304NotModified);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Ciudadanos/5/detail
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}/detail")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CiudadanoDto>> Put(int id, CiudadanoTranVM vm)
        {
            if (id != vm.Ciudadano.CiudadanoId)
            {
                logger.LogWarning($"No coincide el Id del parámetro con el modelo: {id}!={vm.Ciudadano.CiudadanoId}");

                return BadRequest();
            }

            try
            {
                var entidad = await ciudadanoAppService.FindAsync(id);

                if (entidad == null)
                    return NotFound();

                // Establecer campos editables
                /*
                entidad.Nombre = vm.Nombre;
                entidad.ApellidoPaterno = vm.ApellidoPaterno;
                entidad.ApellidoMaterno = vm.ApellidoMaterno;
                entidad.Sexo = vm.Sexo;
                entidad.NacionalidadId = vm.NacionalidadId;
                entidad.TipoDocumentoIdentidadId = vm.TipoDocumentoIdentidadId;
                entidad.NroDocumentoIdentidad = vm.NroDocumentoIdentidad;
                entidad.Celular = vm.Celular;
                entidad.Correo = vm.Correo;
                */

                var ciudadano = mapper.Map<Ciudadano>(vm.Ciudadano);
                var contactos = mapper.Map<List<Contacto>>(vm.Contactos);
                var direcciones = mapper.Map<List<Direccion>>(vm.Direcciones);
                var sintomas = mapper.Map<List<Sintoma>>(vm.Sintomas);
                var situacionesRiesgo = mapper.Map<List<SituacionRiesgo>>(vm.SituacionesRiesgo);

                int r = await ciudadanoAppService.ActualizarAsync(ciudadano, contactos, direcciones, sintomas, situacionesRiesgo);

                if (r > 0)
                    return mapper.Map<CiudadanoDto>(entidad);

                return NoContent();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!await Exists(id))
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido eliminado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return NotFound();
                }
                else
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido modificado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return StatusCode(StatusCodes.Status304NotModified);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/Ciudadanos/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Ciudadano>> DeleteCiudadano(int id)
        {
            try
            {
                var entidad = await ciudadanoAppService.FindAsync(id);
                if (entidad == null)
                {
                    return NotFound();
                }

                await ciudadanoAppService.EliminarAsync(entidad);

                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al eliminar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private async Task<bool> Exists(int id)
        {
            return await ciudadanoAppService.AnyAsync(e => e.CiudadanoId == id);
        }
    }
}
