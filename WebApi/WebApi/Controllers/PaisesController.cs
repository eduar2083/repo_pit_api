﻿using Application.MainModule.Contracts.Services.Common;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Administrador")]
    [ApiController]
    [Route("api/[Controller]")]
    public class PaisesController : ControllerBase
    {
        private readonly IPaisAppService paisAppService;
        private readonly IMapper mapper;
        private readonly ILogger<PaisesController> logger;

        public PaisesController(IPaisAppService paisAppService,
                                IMapper mapper,
                                ILogger<PaisesController> logger)
        {
            this.paisAppService = paisAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Paises
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PaisDto>>> Get()
        {
            try
            {
                return mapper.Map<List<PaisDto>>(await paisAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
