﻿using Application.MainModule.Contracts.Services.Covid19;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Mime;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrador")]
    public class EstadisticasController : ControllerBase
    {
        private readonly IEstadisticaAppService estadisticaAppService;
        private readonly IMapper mapper;
        private readonly ILogger<EstadisticasController> logger;

        public EstadisticasController(IEstadisticaAppService estadisticaAppService,
                                      IMapper mapper,
                                      ILogger<EstadisticasController> logger)
        {
            this.estadisticaAppService = estadisticaAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpPost("filtrar")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EstadisticaDto>> Filtrar(EstadisticaCriteria criteria)
        {
            try
            {
                return await estadisticaAppService.FiltrarAsync(criteria);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
