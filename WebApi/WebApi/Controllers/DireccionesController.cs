﻿using Application.MainModule.Contracts.Services.Covid19;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Covid19;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrador")]
    public class DireccionesController : ControllerBase
    {
        private readonly IDireccionAppService direccionAppService;
        private readonly IMapper mapper;
        private readonly ILogger<DireccionesController> logger;
        private readonly GeometryFactory geometryFactory;

        public DireccionesController(IDireccionAppService direccionAppService,
                                     IMapper mapper,
                                     ILogger<DireccionesController> logger,
                                     GeometryFactory geometryFactory)
        {
            this.direccionAppService = direccionAppService;
            this.mapper = mapper;
            this.logger = logger;
            this.geometryFactory = geometryFactory;
        }

        // GET: api/Direcciones/Cercanos/latitud/-11.9870902/longitud/-77.0732226
        [HttpGet("Confirmados")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<DireccionDto>>> Confirmados(double? Latitud, double? Longitud)
        {
            try
            {
                Point ubicacion = null;
                if (Latitud != null && Longitud != null)
                    ubicacion = geometryFactory.CreatePoint(new Coordinate((double)Longitud, (double)Latitud));

                return mapper.Map<List<DireccionDto>>(await direccionAppService.ConfirmadosAsync(ubicacion));
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}