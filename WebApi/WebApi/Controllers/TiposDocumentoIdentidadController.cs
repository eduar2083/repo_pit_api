﻿using Application.MainModule.Contracts.Services.Common;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Administrador")]
    [ApiController]
    [Route("api/[Controller]")]
    public class TiposDocumentoIdentidadController : ControllerBase
    {
        private readonly ITipoDocumentoIdentidadAppService tipoDocumentoIdentidadAppService;
        private readonly IMapper mapper;
        private readonly ILogger<TiposDocumentoIdentidadController> logger;

        public TiposDocumentoIdentidadController(ITipoDocumentoIdentidadAppService tipoDocumentoIdentidadAppService,
                                                 IMapper mapper,
                                                 ILogger<TiposDocumentoIdentidadController> logger)
        {
            this.tipoDocumentoIdentidadAppService = tipoDocumentoIdentidadAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<TipoDocumentoIdentidadDto>>> Get()
        {
            try
            {
                return mapper.Map<List<TipoDocumentoIdentidadDto>>(await tipoDocumentoIdentidadAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
