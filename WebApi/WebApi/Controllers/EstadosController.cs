﻿using Application.MainModule.Contracts.Services.Covid19;
using AutoMapper;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebApi.ViewModels.Covid19;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrador")]
    public class EstadosController : ControllerBase
    {
        private readonly IEstadoAppService estadoAppService;
        private readonly IMapper mapper;
        private readonly ILogger<EstadosController> logger;

        public EstadosController(IEstadoAppService estadoAppService,
                                 IMapper mapper,
                                 ILogger<EstadosController> logger)
        {
            this.estadoAppService = estadoAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Estados
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<EstadoDto>>> Get()
        {
            try
            {
                return mapper.Map<List<EstadoDto>>(await estadoAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Estados/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EstadoDto>> Get(int id)
        {
            try
            {
                var estado = await estadoAppService.FindAsync(id);

                if (estado == null)
                {
                    return NotFound();
                }

                return mapper.Map<EstadoDto>(estado);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/Estados
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EstadoDto>> Post(EstadoVM vm)
        {
            try
            {
                var entidad = mapper.Map<Estado>(vm);

                await estadoAppService.AddAsync(entidad);

                var dto = mapper.Map<EstadoDto>(entidad);

                return CreatedAtAction(nameof(Get), new { id = dto.Estadoid }, dto);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al crear el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        // PUT: api/Estados/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<EstadoDto>> Put(int id, EstadoVM vm)
        {
            if (id != vm.EstadoId)
            {
                logger.LogWarning($"No coincide el Id del parámetro con el modelo: {id}!={vm.EstadoId}");

                return BadRequest();
            }

            try
            {
                var entidad = await estadoAppService.FindAsync(id);

                if (entidad == null)
                    return NotFound();

                // Establecer campos editables
                entidad.Nombre = vm.Nombre;
                entidad.Descripcion = vm.Descripcion;

                int r = await estadoAppService.UpdateAsync(entidad);
                if (r > 0)
                    return mapper.Map<EstadoDto>(entidad);

                return NoContent();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!await Exists(id))
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido eliminado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return NotFound();
                }
                else
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido modificado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return StatusCode(StatusCodes.Status304NotModified);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/Estados/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Estado>> Delete(int id)
        {
            try
            {
                var entidad = await estadoAppService.FindAsync(id);
                if (entidad == null)
                {
                    return NotFound();
                }

                await estadoAppService.DeleteAsync(entidad);

                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al eliminar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private async Task<bool> Exists(int id)
        {
            return await estadoAppService.AnyAsync(e => e.EstadoId == id);
        }
    }
}
