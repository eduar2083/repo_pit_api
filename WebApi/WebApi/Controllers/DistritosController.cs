﻿using Application.MainModule.Contracts.Services.Common;
using AutoMapper;
using Infraestructure.CrossCutting.Dto.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Administrador")]
    [ApiController]
    [Route("api/[Controller]")]
    public class DistritosController : ControllerBase
    {
        private readonly IDistritoAppService distritoAppService;
        private readonly IMapper mapper;
        private readonly ILogger<DistritosController> logger;

        public DistritosController(IDistritoAppService distritoAppService,
                                   IMapper mapper,
                                   ILogger<DistritosController> logger)
        {
            this.distritoAppService = distritoAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        [HttpGet("{ProvinciaId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<DistritoDto>>> Get(int ProvinciaId)
        {
            try
            {
                return mapper.Map<List<DistritoDto>>(await distritoAppService.ListarPorProvincia(ProvinciaId));
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
