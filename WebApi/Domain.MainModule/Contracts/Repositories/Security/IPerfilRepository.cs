﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Security;

namespace Domain.MainModule.Contracts.Repositories.Security
{
    public interface IPerfilRepository : IGenericRepository<Perfil>
    {
    }
}
