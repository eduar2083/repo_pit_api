﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Covid19;

namespace Domain.MainModule.Contracts.Repositories.Covid19
{
    public interface ISituacionRiesgoRepository : IGenericRepository<SituacionRiesgo>
    {
    }
}
