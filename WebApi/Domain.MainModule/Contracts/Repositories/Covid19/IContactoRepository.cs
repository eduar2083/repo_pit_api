﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Covid19;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Covid19
{
    public interface IContactoRepository : IGenericRepository<Contacto>
    {
        Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId);
    }
}
