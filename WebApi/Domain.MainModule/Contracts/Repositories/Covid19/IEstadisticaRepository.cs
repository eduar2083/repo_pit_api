﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Covid19
{
    public interface IEstadisticaRepository : IGenericRepository<Ciudadano>
    {
        Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c);
    }
}
