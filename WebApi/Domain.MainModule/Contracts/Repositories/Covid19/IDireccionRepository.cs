﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Covid19;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Covid19
{
    public interface IDireccionRepository : IGenericRepository<Direccion>
    {
        Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId);

        Task<List<Direccion>> ConfirmadosAsync(Point ubicacion);
    }
}
