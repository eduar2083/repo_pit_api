﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Common;

namespace Domain.MainModule.Contracts.Repositories.Common
{
    public interface IDepartamentoRepository : IGenericRepository<Departamento>
    {
    }
}
