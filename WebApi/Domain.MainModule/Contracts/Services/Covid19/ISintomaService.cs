﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Covid19;

namespace Domain.MainModule.Contracts.Services.Covid19
{
    public interface ISintomaService : IGenericService<Sintoma>
    {
    }
}
