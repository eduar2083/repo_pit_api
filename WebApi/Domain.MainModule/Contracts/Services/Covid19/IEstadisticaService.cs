﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Covid19
{
    public interface IEstadisticaService : IGenericService<Ciudadano>
    {
        Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c);
    }
}
