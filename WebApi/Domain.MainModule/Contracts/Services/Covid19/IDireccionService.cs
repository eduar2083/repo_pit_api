﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Covid19;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Covid19
{
    public interface IDireccionService : IGenericService<Direccion>
    {
        Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId);

        Task<List<Direccion>> ConfirmadosAsync(Point ubicacion);
    }
}
