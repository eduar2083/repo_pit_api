﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Covid19
{
    public interface ICiudadanoService : IGenericService<Ciudadano>
    {
        Task<int> AgregarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo);

        Task<int> ActualizarAsync(Ciudadano ciudadano, List<Contacto> contactos, List<Direccion> direcciones, List<Sintoma> sintomas, List<SituacionRiesgo> situacionesRiesgo);

        Task<int> EliminarAsync(Ciudadano ciudadano);

        Task<Ciudadano> ObtenerConDetalleAsync(int CiudadanoId);

        Task<List<Ciudadano>> FiltrarAsync(CiudadanoCriteria c);
    }
}
