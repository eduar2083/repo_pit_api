﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Common
{
    public interface IProvinciaService : IGenericService<Provincia>
    {
        Task<List<Provincia>> ListarPorDepartamentoAsync(int DepartamentoId);
    }
}
