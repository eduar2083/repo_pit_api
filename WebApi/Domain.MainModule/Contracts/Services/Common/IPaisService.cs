﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Common;

namespace Domain.MainModule.Contracts.Services.Common
{
    public interface IPaisService : IGenericService<Pais>
    {
    }
}
