﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Security
{
    public interface IUsuarioService : IGenericService<Usuario>
    {
        Task<IEnumerable<Usuario>> ListAllWithProfileAsync();

        Task<Usuario> FindWithProfileAsync(int Id);

        Task<int> ChangePasswordAsync(Usuario usuario);

        Task ChangeProfileAsync(Usuario usuario);

        Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario);
    }
}
