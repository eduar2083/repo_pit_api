﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Security;

namespace Domain.MainModule.Contracts.Services.Security
{
    public interface IPerfilService : IGenericService<Perfil>
    {
    }
}
