﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.MainModule.Contracts.UnitOfWork;
using Infraestructure.Data.Core.Contracts;

namespace Infraestructure.Data.MainModule.UnitOfWork
{
    public class UnitOfWorkMainModuleFactory : IUnitOfWorkMainModuleFactory
    {
        private IDatabaseContext databaseContext;

        public UnitOfWorkMainModuleFactory(IDatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWorkMainModule(databaseContext);
        }
    }
}
