﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Common;
using Domain.Core.Entities.Covid19;
using Domain.Core.Entities.Security;
using Domain.MainModule.Contracts.UnitOfWork;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.UnitOfWork
{
    public class UnitOfWorkMainModule : Core.UnitOfWork.UnitOfWork, IUnitOfWorkMainModule
    {
        private IDatabaseContext databaseContext;

        #region Security
        private IGenericRepository<Perfil> perfilRepository;
        private IGenericRepository<Usuario> usuarioRepository;
        #endregion

        #region Common
        private IGenericRepository<Pais> paisRepository;
        private IGenericRepository<TipoDocumentoIdentidad> tipoDocumentoIdentidadRepository;
        private IGenericRepository<Departamento> departamentoRepository;
        private IGenericRepository<Provincia> provinciaRepository;
        private IGenericRepository<Distrito> distritoRepository;
        #endregion

        #region Covid
        private IGenericRepository<Ciudadano> ciudadanoRepository;
        #endregion

        public UnitOfWorkMainModule(IDatabaseContext databaseContext) : base(databaseContext)
        {
            this.databaseContext = databaseContext;

            perfilRepository = new GenericRepository<Perfil>(databaseContext);
            usuarioRepository = new GenericRepository<Usuario>(databaseContext);

            paisRepository = new GenericRepository<Pais>(databaseContext);
            tipoDocumentoIdentidadRepository = new GenericRepository<TipoDocumentoIdentidad>(databaseContext);
            departamentoRepository = new GenericRepository<Departamento>(databaseContext);
            departamentoRepository = new GenericRepository<Departamento>(databaseContext);
            provinciaRepository = new GenericRepository<Provincia>(databaseContext);
            distritoRepository = new GenericRepository<Distrito>(databaseContext);

            ciudadanoRepository = new GenericRepository<Ciudadano>(databaseContext);
        }

        #region Security
        public IGenericRepository<Perfil> PerfilRepository => perfilRepository ?? new GenericRepository<Perfil>(databaseContext);

        public IGenericRepository<Usuario> UsuarioRepository => usuarioRepository ?? new GenericRepository<Usuario>(databaseContext);
        #endregion

        #region Common
        public IGenericRepository<Pais> PaisRepository => paisRepository ?? new GenericRepository<Pais>(databaseContext);

        public IGenericRepository<TipoDocumentoIdentidad> TipoDocumentoIdentidadRepository => tipoDocumentoIdentidadRepository ?? new GenericRepository<TipoDocumentoIdentidad>(databaseContext);

        public IGenericRepository<Departamento> DepartamentoRepository => departamentoRepository ?? new GenericRepository<Departamento>(databaseContext);

        public IGenericRepository<Provincia> ProvinciaRepository => provinciaRepository ?? new GenericRepository<Provincia>(databaseContext);

        public IGenericRepository<Distrito> DistritoRepository => distritoRepository ?? new GenericRepository<Distrito>(databaseContext);
        #endregion

        #region Covid
        public IGenericRepository<Ciudadano> CiudadanoRepository => ciudadanoRepository ?? new GenericRepository<Ciudadano>(databaseContext);
        #endregion
    }
}
