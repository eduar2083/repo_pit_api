﻿using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Security
{
    public class PerfilConfig : IEntityTypeConfiguration<Perfil>
    {
        public void Configure(EntityTypeBuilder<Perfil> entity)
        {
            entity.ToTable("Perfil", Schemas.SEGURIDAD);

            entity.HasKey(t => t.PerfilId);

            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}
