﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class DireccionConfig : IEntityTypeConfiguration<Direccion>
    {
        public void Configure(EntityTypeBuilder<Direccion> entity)
        {
            entity.ToTable("Direccion", Schemas.COVID);

            entity.HasKey(t => t.DireccionId);

            entity.Property(t => t.Ubicacion)
                .IsRequired();

            entity.HasOne(t => t.Distrito)
                .WithMany(t => t.Direcciones)
                .HasForeignKey(t => t.DistritoId)
                .OnDelete(DeleteBehavior.NoAction);

            entity.HasIndex(t => t.DistritoId)
                .HasName("IX_Direccion_DistritoId")
                .IsClustered(false);

            entity.Property(t => t.Descripcion)
                .HasMaxLength(150)
                .IsRequired();

            entity.HasOne(t => t.Ciudadano)
                .WithMany(t => t.Direcciones)
                .HasForeignKey(t => t.CiudadanoId)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }
}
