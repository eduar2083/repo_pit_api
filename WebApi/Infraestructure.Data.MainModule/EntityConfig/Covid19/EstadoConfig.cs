﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class EstadoConfig : IEntityTypeConfiguration<Estado>
    {
        public void Configure(EntityTypeBuilder<Estado> builder)
        {
            builder.ToTable("Estado", Schemas.COVID);

            builder.HasKey(t => t.EstadoId);

            builder.Property(t => t.Nombre)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(t => t.Descripcion)
                .HasMaxLength(250);

            builder.HasIndex(t => t.Nombre)
                .HasName("IX_Estado_Nombre")
                .IsUnique(true)
                .IsClustered(false);
        }
    }
}
