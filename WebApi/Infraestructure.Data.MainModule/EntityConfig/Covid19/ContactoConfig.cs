﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class ContactoConfig : IEntityTypeConfiguration<Contacto>
    {
        public void Configure(EntityTypeBuilder<Contacto> entity)
        {
            entity.ToTable("Contacto", Schemas.COVID);

            entity.HasKey(t => t.ContactoId);

            entity.Property(t => t.NombreContacto)
                .HasMaxLength(200)
                .IsRequired();

            entity.HasOne(t => t.Ciudadano)
                .WithMany(t => t.Contactos)
                .HasForeignKey(t => t.CiudadanoId)
                .OnDelete(DeleteBehavior.NoAction);

            entity.HasIndex(t => t.CiudadanoId)
                .HasName("IX_Conctacto_CiudadanoId");

            entity.Property(t => t.Parentesco)
                .HasMaxLength(150)
                .IsRequired();

            entity.Property(t => t.Telefono)
                .HasMaxLength(9);

            entity.Property(t => t.Correo)
                .HasMaxLength(100);
        }
    }
}
