﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class SintomaConfig : IEntityTypeConfiguration<Sintoma>
    {
        public void Configure(EntityTypeBuilder<Sintoma> builder)
        {
            builder.ToTable("Sintoma", Schemas.COVID);

            builder.HasKey(t => new { t.SintomaId, t.CiudadanoId });

            builder.HasOne(t => t.Ciudadano)
                .WithMany(t => t.Sintomas)
                .HasForeignKey(t => t.CiudadanoId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
