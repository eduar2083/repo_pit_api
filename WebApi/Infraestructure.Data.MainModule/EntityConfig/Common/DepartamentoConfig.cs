﻿using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructure.Data.MainModule.EntityConfig.Common
{
    public class DepartamentoConfig : IEntityTypeConfiguration<Departamento>
    {
        public void Configure(EntityTypeBuilder<Departamento> entity)
        {
            entity.ToTable("Departamento", Schemas.COMMON);

            entity.HasKey(t => t.DepartamentoId);

            entity.Property(t => t.Ubigeo)
                .HasMaxLength(2)
                .HasColumnType("char(2)")
                .IsRequired();

            entity.Property(t => t.Nombre)
                .HasMaxLength(50)
                .IsRequired();

            entity.HasIndex(t => t.Ubigeo)
                .HasName("IX_Departamento_Ubigeo")
                .IsUnique()
                .IsClustered(false);
        }
    }
}
