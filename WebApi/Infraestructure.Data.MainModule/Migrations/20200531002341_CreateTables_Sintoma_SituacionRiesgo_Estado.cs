﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class CreateTables_Sintoma_SituacionRiesgo_Estado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EstadoId",
                schema: "covid",
                table: "Ciudadano",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Estado",
                schema: "covid",
                columns: table => new
                {
                    EstadoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Descripcion = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estado", x => x.EstadoId);
                });

            migrationBuilder.CreateTable(
                name: "Sintoma",
                schema: "covid",
                columns: table => new
                {
                    SintomaId = table.Column<int>(nullable: false),
                    CiudadanoId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sintoma", x => new { x.SintomaId, x.CiudadanoId });
                    table.ForeignKey(
                        name: "FK_Sintoma_Ciudadano_CiudadanoId",
                        column: x => x.CiudadanoId,
                        principalSchema: "covid",
                        principalTable: "Ciudadano",
                        principalColumn: "CiudadanoId");
                });

            migrationBuilder.CreateTable(
                name: "SituacionRiesgo",
                schema: "covid",
                columns: table => new
                {
                    SituacionRiesgoId = table.Column<int>(nullable: false),
                    CiudadanoId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SituacionRiesgo", x => new { x.SituacionRiesgoId, x.CiudadanoId });
                    table.ForeignKey(
                        name: "FK_SituacionRiesgo_Ciudadano_CiudadanoId",
                        column: x => x.CiudadanoId,
                        principalSchema: "covid",
                        principalTable: "Ciudadano",
                        principalColumn: "CiudadanoId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ciudadano_EstadoId",
                schema: "covid",
                table: "Ciudadano",
                column: "EstadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Estado_Nombre",
                schema: "covid",
                table: "Estado",
                column: "Nombre",
                unique: true)
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Sintoma_CiudadanoId",
                schema: "covid",
                table: "Sintoma",
                column: "CiudadanoId");

            migrationBuilder.CreateIndex(
                name: "IX_SituacionRiesgo_CiudadanoId",
                schema: "covid",
                table: "SituacionRiesgo",
                column: "CiudadanoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ciudadano_Estado_EstadoId",
                schema: "covid",
                table: "Ciudadano",
                column: "EstadoId",
                principalSchema: "covid",
                principalTable: "Estado",
                principalColumn: "EstadoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ciudadano_Estado_EstadoId",
                schema: "covid",
                table: "Ciudadano");

            migrationBuilder.DropTable(
                name: "Estado",
                schema: "covid");

            migrationBuilder.DropTable(
                name: "Sintoma",
                schema: "covid");

            migrationBuilder.DropTable(
                name: "SituacionRiesgo",
                schema: "covid");

            migrationBuilder.DropIndex(
                name: "IX_Ciudadano_EstadoId",
                schema: "covid",
                table: "Ciudadano");

            migrationBuilder.DropColumn(
                name: "EstadoId",
                schema: "covid",
                table: "Ciudadano");
        }
    }
}
