﻿using Infraestructure.CrossCutting.Utilities.Extensions;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class Create_Procedure_USP_ESTADISTICAS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblyByName("Infraestructure.Data.MainModule");
            var sql = assembly.ReadFileAsync("USP_ESTADISTICAS.sql");

            migrationBuilder.Sql(sql.Result);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP PROCEDURE covid.USP_ESTADISTICAS");
        }
    }
}
