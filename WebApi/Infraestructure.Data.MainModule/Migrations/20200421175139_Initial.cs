﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "common");

            migrationBuilder.EnsureSchema(
                name: "seguridad");

            migrationBuilder.CreateTable(
                name: "Departamento",
                schema: "common",
                columns: table => new
                {
                    DepartamentoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ubigeo = table.Column<string>(type: "char(2)", unicode: false, maxLength: 2, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departamento", x => x.DepartamentoId);
                });

            migrationBuilder.CreateTable(
                name: "Pais",
                schema: "common",
                columns: table => new
                {
                    PaisId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CodigoISO = table.Column<string>(type: "char(2)", unicode: false, maxLength: 2, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pais", x => x.PaisId);
                });

            migrationBuilder.CreateTable(
                name: "TipoDocumento",
                schema: "common",
                columns: table => new
                {
                    TipoDocumentoIdentidadId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DescripcionLarga = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DescripcionCorta = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDocumento", x => x.TipoDocumentoIdentidadId);
                });

            migrationBuilder.CreateTable(
                name: "Perfil",
                schema: "seguridad",
                columns: table => new
                {
                    PerfilId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Eliminado = table.Column<bool>(nullable: false),
                    Audit_UsuarioCreacion = table.Column<string>(unicode: false, maxLength: 50, nullable: false, defaultValueSql: "SYSTEM_USER"),
                    Audit_FechaCreacion = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    Audit_UsuarioActualizacion = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Audit_FechaActualizacion = table.Column<DateTime>(nullable: true),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Perfil", x => x.PerfilId);
                });

            migrationBuilder.CreateTable(
                name: "Provincia",
                schema: "common",
                columns: table => new
                {
                    ProvinciaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ubigeo = table.Column<string>(type: "char(4)", unicode: false, maxLength: 4, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    DepartamentoId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provincia", x => x.ProvinciaId);
                    table.ForeignKey(
                        name: "FK_Provincia_Departamento_DepartamentoId",
                        column: x => x.DepartamentoId,
                        principalSchema: "common",
                        principalTable: "Departamento",
                        principalColumn: "DepartamentoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                schema: "seguridad",
                columns: table => new
                {
                    PerfilId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false),
                    Audit_UsuarioCreacion = table.Column<string>(unicode: false, maxLength: 50, nullable: false, defaultValueSql: "SYSTEM_USER"),
                    Audit_FechaCreacion = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    Audit_UsuarioActualizacion = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Audit_FechaActualizacion = table.Column<DateTime>(nullable: true),
                    UsuarioId = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ApellidoPaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ApellidoMaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Telefono = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    Username = table.Column<string>(unicode: false, maxLength: 25, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 512, nullable: false),
                    Estado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.PerfilId);
                    table.ForeignKey(
                        name: "FK_Usuario_Perfil_PerfilId",
                        column: x => x.PerfilId,
                        principalSchema: "seguridad",
                        principalTable: "Perfil",
                        principalColumn: "PerfilId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Distrito",
                schema: "common",
                columns: table => new
                {
                    DistritoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ubigeo = table.Column<string>(type: "char(6)", unicode: false, maxLength: 6, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    ProvinciaId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Distrito", x => x.DistritoId);
                    table.ForeignKey(
                        name: "FK_Distrito_Provincia_ProvinciaId",
                        column: x => x.ProvinciaId,
                        principalSchema: "common",
                        principalTable: "Provincia",
                        principalColumn: "ProvinciaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Departamento_Ubigeo",
                schema: "common",
                table: "Departamento",
                column: "Ubigeo",
                unique: true)
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Distrito_ProvinciaId",
                schema: "common",
                table: "Distrito",
                column: "ProvinciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Distrito_Ubigeo",
                schema: "common",
                table: "Distrito",
                column: "Ubigeo",
                unique: true)
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Pais_CodigoISO",
                schema: "common",
                table: "Pais",
                column: "CodigoISO",
                unique: true)
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Provincia_DepartamentoId",
                schema: "common",
                table: "Provincia",
                column: "DepartamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_Provincia_Ubigeo",
                schema: "common",
                table: "Provincia",
                column: "Ubigeo",
                unique: true)
                .Annotation("SqlServer:Clustered", false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Distrito",
                schema: "common");

            migrationBuilder.DropTable(
                name: "Pais",
                schema: "common");

            migrationBuilder.DropTable(
                name: "TipoDocumento",
                schema: "common");

            migrationBuilder.DropTable(
                name: "Usuario",
                schema: "seguridad");

            migrationBuilder.DropTable(
                name: "Provincia",
                schema: "common");

            migrationBuilder.DropTable(
                name: "Perfil",
                schema: "seguridad");

            migrationBuilder.DropTable(
                name: "Departamento",
                schema: "common");
        }
    }
}
