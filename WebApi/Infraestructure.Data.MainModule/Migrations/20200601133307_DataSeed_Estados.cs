﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class DataSeed_Estados : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "covid",
                table: "Estado",
                columns: new[] { "EstadoId", "Descripcion", "Eliminado", "Nombre" },
                values: new object[,]
                {
                    { 1, "Persona que no es sospechosa de contagio", false, "No sospechoso" },
                    { 2, "Persona que es sospechosa de contagio", false, "Sospechoso" },
                    { 3, "Persona que ha sido confirmada de contagio", false, "Confirmado" },
                    { 4, "Persona que ha sido descartada de contagio", false, "Descartado" },
                    { 5, "Persona que se ha recuperado de un contagio", false, "Recuperado" },
                    { 6, "Persona que falleció por contagio", false, "Fallecido" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                schema: "covid",
                table: "Estado",
                keyColumn: "EstadoId",
                keyValue: 6);
        }
    }
}
