﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class CreateTable_Ciudano_Contacto_Direccion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "covid");

            migrationBuilder.CreateTable(
                name: "Ciudadano",
                schema: "covid",
                columns: table => new
                {
                    CiudadanoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Eliminado = table.Column<bool>(nullable: false),
                    Audit_UsuarioCreacion = table.Column<string>(unicode: false, maxLength: 50, nullable: false, defaultValueSql: "SYSTEM_USER"),
                    Audit_FechaCreacion = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()"),
                    Audit_UsuarioActualizacion = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Audit_FechaActualizacion = table.Column<DateTime>(nullable: true),
                    ApellidoPaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ApellidoMaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Nombre = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Sexo = table.Column<bool>(nullable: false),
                    NacionalidadId = table.Column<int>(nullable: false),
                    TipoDocumentoIdentidadId = table.Column<int>(nullable: false),
                    NroDocumentoIdentidad = table.Column<string>(unicode: false, maxLength: 12, nullable: false),
                    Celular = table.Column<string>(unicode: false, maxLength: 9, nullable: false),
                    Correo = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    FechaNacimiento = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ciudadano", x => x.CiudadanoId);
                    table.ForeignKey(
                        name: "FK_Ciudadano_Pais_NacionalidadId",
                        column: x => x.NacionalidadId,
                        principalSchema: "common",
                        principalTable: "Pais",
                        principalColumn: "PaisId");
                    table.ForeignKey(
                        name: "FK_Ciudadano_TipoDocumento_TipoDocumentoIdentidadId",
                        column: x => x.TipoDocumentoIdentidadId,
                        principalSchema: "common",
                        principalTable: "TipoDocumento",
                        principalColumn: "TipoDocumentoIdentidadId");
                });

            migrationBuilder.CreateTable(
                name: "Contacto",
                schema: "covid",
                columns: table => new
                {
                    ContactoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CiudadanoId = table.Column<int>(nullable: false),
                    NombreContacto = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Parentesco = table.Column<string>(unicode: false, maxLength: 150, nullable: false),
                    Telefono = table.Column<string>(unicode: false, maxLength: 9, nullable: true),
                    Correo = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacto", x => x.ContactoId);
                    table.ForeignKey(
                        name: "FK_Contacto_Ciudadano_CiudadanoId",
                        column: x => x.CiudadanoId,
                        principalSchema: "covid",
                        principalTable: "Ciudadano",
                        principalColumn: "CiudadanoId");
                });

            migrationBuilder.CreateTable(
                name: "Direccion",
                schema: "covid",
                columns: table => new
                {
                    DireccionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistritoId = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(unicode: false, maxLength: 150, nullable: false),
                    CiudadanoId = table.Column<int>(nullable: false),
                    Eliminado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Direccion", x => x.DireccionId);
                    table.ForeignKey(
                        name: "FK_Direccion_Ciudadano_CiudadanoId",
                        column: x => x.CiudadanoId,
                        principalSchema: "covid",
                        principalTable: "Ciudadano",
                        principalColumn: "CiudadanoId");
                    table.ForeignKey(
                        name: "FK_Direccion_Distrito_DistritoId",
                        column: x => x.DistritoId,
                        principalSchema: "common",
                        principalTable: "Distrito",
                        principalColumn: "DistritoId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ciudadano_NacionalidadId",
                schema: "covid",
                table: "Ciudadano",
                column: "NacionalidadId")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Ciudadano_TipoDocumentoIdentidadId",
                schema: "covid",
                table: "Ciudadano",
                column: "TipoDocumentoIdentidadId")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Ciudadano_TipoDocumentoIdentidadId_NroDocumentoIdentidad",
                schema: "covid",
                table: "Ciudadano",
                columns: new[] { "TipoDocumentoIdentidadId", "NroDocumentoIdentidad" },
                unique: true)
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Conctacto_CiudadanoId",
                schema: "covid",
                table: "Contacto",
                column: "CiudadanoId");

            migrationBuilder.CreateIndex(
                name: "IX_Direccion_CiudadanoId",
                schema: "covid",
                table: "Direccion",
                column: "CiudadanoId");

            migrationBuilder.CreateIndex(
                name: "IX_Direccion_DistritoId",
                schema: "covid",
                table: "Direccion",
                column: "DistritoId")
                .Annotation("SqlServer:Clustered", false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacto",
                schema: "covid");

            migrationBuilder.DropTable(
                name: "Direccion",
                schema: "covid");

            migrationBuilder.DropTable(
                name: "Ciudadano",
                schema: "covid");
        }
    }
}
