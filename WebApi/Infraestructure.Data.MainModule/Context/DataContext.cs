﻿using Domain.Core.Entities.Common;
using Domain.Core.Entities.Covid19;
using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Infraestructure.Data.Core.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Context
{
    public class DataContext : DbContext, IDatabaseContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureBaseConfig();
            modelBuilder.ConfigureQueryFilters();
            modelBuilder.ConfigureEntityConfig();
            modelBuilder.ConfigureDataSeed();
        }

        public override int SaveChanges()
        {
            InterceptSaveChanges();
            return base.SaveChanges();
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            InterceptSaveChanges();
            return await base.SaveChangesAsync(cancellationToken);
        }

        public override DatabaseFacade Database => base.Database;

        private void InterceptSaveChanges()
        {
            SoftDeleted();
            ConfigureEdit();
        }
        
        private void SoftDeleted()
        {
            foreach (var entry in ChangeTracker.Entries()
                                  .Where(t => t.State == EntityState.Deleted &&
                                              t.Metadata.GetProperties().Any(p => p.Name == CamposAuditoria.ELIMINADO)))
            {
                entry.State = EntityState.Unchanged;
                entry.CurrentValues[CamposAuditoria.ELIMINADO] = true;
            }
        }

        private void ConfigureEdit()
        {
            foreach (var entry in ChangeTracker.Entries()
                                  .Where(t => t.State == EntityState.Modified &&
                                              t.Metadata.GetProperties().Any(p => p.Name == CamposAuditoria.AUDIT_USUARIO_CREACION || p.Name == CamposAuditoria.AUDIT_FECHA_CREACION)))
            {
                entry.Property(CamposAuditoria.AUDIT_USUARIO_CREACION).IsModified = false;
                entry.Property(CamposAuditoria.AUDIT_FECHA_CREACION).IsModified = false;
            }

            foreach (var entry in ChangeTracker.Entries()
                                  .Where(t => t.State == EntityState.Modified &&
                                              t.Metadata.GetProperties().Any(p => p.Name == CamposAuditoria.AUDIT_FECHA_ACTUALIZACION)))
            {
                entry.CurrentValues[CamposAuditoria.AUDIT_FECHA_ACTUALIZACION] = DateTime.Now;
            }
        }

        public DbSet<Perfil> Perfiles { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<TipoDocumentoIdentidad> TiposDocumentoIdentidad { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Ciudadano> Ciudadanos { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
    }
}
