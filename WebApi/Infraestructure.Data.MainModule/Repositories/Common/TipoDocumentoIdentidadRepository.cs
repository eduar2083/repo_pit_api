﻿using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Repositories.Common;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.Repositories.Common
{
    public class TipoDocumentoIdentidadRepository : GenericRepository<TipoDocumentoIdentidad>, ITipoDocumentoIdentidadRepository
    {
        public TipoDocumentoIdentidadRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
