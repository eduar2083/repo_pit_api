﻿using Domain.Core.Entities.Security;
using Domain.MainModule.Contracts.Repositories.Security;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.Repositories.Security
{
    public class PerfilRepository : GenericRepository<Perfil>, IPerfilRepository
    {
        public PerfilRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
