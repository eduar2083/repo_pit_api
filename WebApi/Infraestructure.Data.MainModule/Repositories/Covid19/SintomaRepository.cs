﻿using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class SintomaRepository : GenericRepository<Sintoma>, ISintomaRepository
    {
        public SintomaRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
