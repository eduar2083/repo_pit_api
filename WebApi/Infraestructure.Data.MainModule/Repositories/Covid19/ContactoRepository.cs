﻿using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class ContactoRepository : GenericRepository<Contacto>, IContactoRepository
    {
        public ContactoRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId)
        {
            return await base.QueryableAll()
                    .Where(t => t.CiudadanoId == CiudadanoId)
                    .ToListAsync();
        }
    }
}
